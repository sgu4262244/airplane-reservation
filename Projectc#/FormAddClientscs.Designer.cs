﻿namespace Projectc_
{
    partial class FormAddClientscs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAddClientscs));
            btnCancel = new Button();
            btnAdd = new Button();
            txtPhoneNumber = new TextBox();
            label11 = new Label();
            txtIdCitizen = new TextBox();
            label10 = new Label();
            dateTimePickerDateOfBirth = new DateTimePicker();
            label9 = new Label();
            txtGender = new TextBox();
            label7 = new Label();
            txtCustomerName = new TextBox();
            Name = new Label();
            txtCustomerFamily = new TextBox();
            label5 = new Label();
            label4 = new Label();
            panel1 = new Panel();
            panel1.SuspendLayout();
            SuspendLayout();
            // 
            // btnCancel
            // 
            btnCancel.Font = new Font("Segoe UI", 10.8F, FontStyle.Bold, GraphicsUnit.Point);
            btnCancel.Image = (Image)resources.GetObject("btnCancel.Image");
            btnCancel.ImageAlign = ContentAlignment.MiddleLeft;
            btnCancel.Location = new Point(286, 429);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new Size(140, 47);
            btnCancel.TabIndex = 38;
            btnCancel.Text = "    Cancel";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Click += btnCancel_Click;
            // 
            // btnAdd
            // 
            btnAdd.Font = new Font("Segoe UI", 10.8F, FontStyle.Bold, GraphicsUnit.Point);
            btnAdd.Image = (Image)resources.GetObject("btnAdd.Image");
            btnAdd.ImageAlign = ContentAlignment.MiddleLeft;
            btnAdd.Location = new Point(55, 429);
            btnAdd.Name = "btnAdd";
            btnAdd.Size = new Size(140, 47);
            btnAdd.TabIndex = 37;
            btnAdd.Text = "    Add";
            btnAdd.UseVisualStyleBackColor = true;
            // 
            // txtPhoneNumber
            // 
            txtPhoneNumber.Location = new Point(197, 375);
            txtPhoneNumber.Name = "txtPhoneNumber";
            txtPhoneNumber.Size = new Size(278, 27);
            txtPhoneNumber.TabIndex = 36;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label11.ForeColor = Color.White;
            label11.Location = new Point(31, 379);
            label11.Name = "label11";
            label11.Size = new Size(130, 23);
            label11.TabIndex = 35;
            label11.Text = "Phone Number";
            // 
            // txtIdCitizen
            // 
            txtIdCitizen.Location = new Point(197, 315);
            txtIdCitizen.Name = "txtIdCitizen";
            txtIdCitizen.Size = new Size(278, 27);
            txtIdCitizen.TabIndex = 34;
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label10.ForeColor = Color.White;
            label10.Location = new Point(31, 319);
            label10.Name = "label10";
            label10.Size = new Size(86, 23);
            label10.TabIndex = 33;
            label10.Text = "Id Citizen";
            // 
            // dateTimePickerDateOfBirth
            // 
            dateTimePickerDateOfBirth.Location = new Point(197, 252);
            dateTimePickerDateOfBirth.Name = "dateTimePickerDateOfBirth";
            dateTimePickerDateOfBirth.Size = new Size(278, 27);
            dateTimePickerDateOfBirth.TabIndex = 32;
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label9.ForeColor = Color.White;
            label9.Location = new Point(31, 256);
            label9.Name = "label9";
            label9.Size = new Size(115, 23);
            label9.TabIndex = 31;
            label9.Text = "Date of birth";
            // 
            // txtGender
            // 
            txtGender.Location = new Point(197, 189);
            txtGender.Name = "txtGender";
            txtGender.Size = new Size(278, 27);
            txtGender.TabIndex = 30;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label7.ForeColor = Color.White;
            label7.Location = new Point(31, 189);
            label7.Name = "label7";
            label7.Size = new Size(68, 23);
            label7.TabIndex = 29;
            label7.Text = "Gender";
            // 
            // txtCustomerName
            // 
            txtCustomerName.Location = new Point(197, 131);
            txtCustomerName.Name = "txtCustomerName";
            txtCustomerName.Size = new Size(278, 27);
            txtCustomerName.TabIndex = 28;
            // 
            // Name
            // 
            Name.AutoSize = true;
            Name.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            Name.ForeColor = Color.White;
            Name.Location = new Point(31, 132);
            Name.Name = "Name";
            Name.Size = new Size(139, 23);
            Name.TabIndex = 27;
            Name.Text = "Customer Name";
            // 
            // txtCustomerFamily
            // 
            txtCustomerFamily.Location = new Point(197, 71);
            txtCustomerFamily.Name = "txtCustomerFamily";
            txtCustomerFamily.Size = new Size(278, 27);
            txtCustomerFamily.TabIndex = 26;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label5.ForeColor = Color.White;
            label5.Location = new Point(31, 75);
            label5.Name = "label5";
            label5.Size = new Size(143, 23);
            label5.TabIndex = 24;
            label5.Text = "Customer family";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Segoe UI", 15F, FontStyle.Bold, GraphicsUnit.Point);
            label4.ForeColor = Color.White;
            label4.Location = new Point(153, 8);
            label4.Name = "label4";
            label4.Size = new Size(184, 35);
            label4.TabIndex = 25;
            label4.Text = "Add Customer";
            // 
            // panel1
            // 
            panel1.Controls.Add(label4);
            panel1.Location = new Point(3, 1);
            panel1.Name = "panel1";
            panel1.Size = new Size(497, 49);
            panel1.TabIndex = 39;
            // 
            // FormAddClientscs
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.Black;
            ClientSize = new Size(501, 500);
            Controls.Add(panel1);
            Controls.Add(btnCancel);
            Controls.Add(btnAdd);
            Controls.Add(txtPhoneNumber);
            Controls.Add(label11);
            Controls.Add(txtIdCitizen);
            Controls.Add(label10);
            Controls.Add(dateTimePickerDateOfBirth);
            Controls.Add(label9);
            Controls.Add(txtGender);
            Controls.Add(label7);
            Controls.Add(txtCustomerName);
            Controls.Add(Name);
            Controls.Add(txtCustomerFamily);
            Controls.Add(label5);
            FormBorderStyle = FormBorderStyle.None;
            StartPosition = FormStartPosition.CenterScreen;
            Text = "FormAddClientscs";
            Load += FormAddClientscs_Load;
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnCancel;
        private Button btnAdd;
        private TextBox txtPhoneNumber;
        private Label label11;
        private TextBox txtIdCitizen;
        private Label label10;
        private DateTimePicker dateTimePickerDateOfBirth;
        private Label label9;
        private TextBox txtGender;
        private Label label7;
        private TextBox txtCustomerName;
        private Label Name;
        private TextBox txtCustomerFamily;
        private Label label5;
        private Label label4;
        private Panel panel1;
    }
}