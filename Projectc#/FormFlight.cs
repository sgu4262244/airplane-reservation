﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projectc_
{
    public partial class Form_Flight : System.Windows.Forms.Form
    {
        public Form_Flight()
        {
            InitializeComponent();
        }



        private void Form_Flight_Load(object sender, EventArgs e)
        {

        }

        private void cbbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbbType.SelectedItem.ToString() == "Onway")
            {
                dtpReturnTime.Enabled = false;
            }
            else { dtpReturnTime.Enabled = true; }
        }

        private void label7_Click(object sender, EventArgs e)
        {
            MessageBox.Show("\r\n1. Vui lòng thực hiện yêu cầu đổi lịch từ menu \"2. Đơn hàng\".\r\nToàn bộ quá trình đổi lịch sẽ tuân theo chính sách của hãng hàng không.\r\n3. Đổi lịch có nghĩa sẽ có sự khác biệt về giá vé.\r\n4. Xin lưu ý rằng sau khi vé của bạn đã được đổi lịch thành công, sự khác biệt về giá vé đã được thanh toán sẽ không được hoàn trả.\r\n5. Mọi yêu cầu đổi lịch nên được thực hiện thông qua trang web Airpaz hoặc ứng dụng Airpaz (phiên bản mới nhất được cập nhật).\r\n6. Airpaz không chịu trách nhiệm về bất kỳ vi phạm nào đối với các điều khoản và điều kiện này.\r\n7. Các điều khoản và điều kiện này có thể thay đổi theo thời gian mà không cần thông báo trước.",
                "Điều khoản và Điều kiện Đổi lịch bay",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }



        private void btnBookNow_Click_1(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
