﻿namespace Projectc_
{
    partial class Form_DashBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_DashBoard));
            label1 = new Label();
            panel1 = new Panel();
            panel13 = new Panel();
            btnAddNewWidgets = new Button();
            label2 = new Label();
            textboxsearch = new TextBox();
            panel12 = new Panel();
            panel5 = new Panel();
            PercentTotalRevenue = new Label();
            pictureBoxTotalRevenue = new PictureBox();
            CounterTotalRevenue = new Label();
            pictureBox6 = new PictureBox();
            label13 = new Label();
            panel2 = new Panel();
            PercentWastingList = new Label();
            pictureBoxWastingList = new PictureBox();
            CounterWastingList = new Label();
            pictureBox1 = new PictureBox();
            label5 = new Label();
            panel4 = new Panel();
            PercentCompletedFlights = new Label();
            pictureBoxCompletedFlights = new PictureBox();
            CounterCompletedFlights = new Label();
            pictureBox4 = new PictureBox();
            label10 = new Label();
            panel3 = new Panel();
            panel8 = new Panel();
            panel22 = new Panel();
            label32 = new Label();
            CounterSales = new Label();
            panel21 = new Panel();
            label15 = new Label();
            CounterViews = new Label();
            pictureBox8 = new PictureBox();
            pictureBox7 = new PictureBox();
            panel7 = new Panel();
            label4 = new Label();
            btnExport = new Button();
            label3 = new Label();
            panel6 = new Panel();
            panel11 = new Panel();
            label21 = new Label();
            label20 = new Label();
            label19 = new Label();
            panel10 = new Panel();
            label14 = new Label();
            DepartureDate = new ComboBox();
            dateTimePicker1 = new DateTimePicker();
            BookingTypes = new ComboBox();
            DateRange = new ComboBox();
            panel9 = new Panel();
            panel20 = new Panel();
            CounterPeopleFlight1 = new Label();
            CounterPeopleFlight2 = new Label();
            panel19 = new Panel();
            pictureBox14 = new PictureBox();
            DestinationSymbolFlight2 = new Label();
            label33 = new Label();
            DepartureSympolCountry2 = new Label();
            label42 = new Label();
            pictureBox15 = new PictureBox();
            pictureBox16 = new PictureBox();
            panel18 = new Panel();
            pictureBox11 = new PictureBox();
            DepartureSympolCountry1 = new Label();
            label28 = new Label();
            label29 = new Label();
            pictureBox13 = new PictureBox();
            pictureBox12 = new PictureBox();
            DestinationSymbolFlight1 = new Label();
            panel17 = new Panel();
            TimeFlightDestination1 = new Label();
            CountryFlightDestination1 = new Label();
            TimeFlightDestination2 = new Label();
            CountryFlightDestination2 = new Label();
            panel16 = new Panel();
            DateFlights1 = new Label();
            DateFlights2 = new Label();
            panel15 = new Panel();
            CountryFlightDeparture2 = new Label();
            TimeFlightDeparture2 = new Label();
            pictureBoxDestinationFlight2 = new PictureBox();
            panel14 = new Panel();
            CountryFlightDeparture1 = new Label();
            TimeFlightDeparture1 = new Label();
            pictureBoxDestinationFLight1 = new PictureBox();
            panel1.SuspendLayout();
            panel13.SuspendLayout();
            panel12.SuspendLayout();
            panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBoxTotalRevenue).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox6).BeginInit();
            panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBoxWastingList).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBoxCompletedFlights).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox4).BeginInit();
            panel3.SuspendLayout();
            panel8.SuspendLayout();
            panel22.SuspendLayout();
            panel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox8).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox7).BeginInit();
            panel6.SuspendLayout();
            panel11.SuspendLayout();
            panel10.SuspendLayout();
            panel9.SuspendLayout();
            panel20.SuspendLayout();
            panel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox14).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox15).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox16).BeginInit();
            panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox11).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox13).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox12).BeginInit();
            panel17.SuspendLayout();
            panel16.SuspendLayout();
            panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBoxDestinationFlight2).BeginInit();
            panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBoxDestinationFLight1).BeginInit();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Sylfaen", 18F, FontStyle.Bold, GraphicsUnit.Point);
            label1.ForeColor = SystemColors.ControlText;
            label1.Location = new Point(6, 5);
            label1.Name = "label1";
            label1.Size = new Size(162, 39);
            label1.TabIndex = 0;
            label1.Text = "DashBoard";
            // 
            // panel1
            // 
            panel1.Anchor = AnchorStyles.Top;
            panel1.Controls.Add(panel13);
            panel1.Controls.Add(panel12);
            panel1.Controls.Add(panel3);
            panel1.Location = new Point(2, 3);
            panel1.Name = "panel1";
            panel1.Size = new Size(1448, 547);
            panel1.TabIndex = 1;
            // 
            // panel13
            // 
            panel13.Anchor = AnchorStyles.Top;
            panel13.Controls.Add(label1);
            panel13.Controls.Add(btnAddNewWidgets);
            panel13.Controls.Add(label2);
            panel13.Controls.Add(textboxsearch);
            panel13.Location = new Point(0, 3);
            panel13.Name = "panel13";
            panel13.Size = new Size(1441, 86);
            panel13.TabIndex = 3;
            // 
            // btnAddNewWidgets
            // 
            btnAddNewWidgets.FlatAppearance.BorderSize = 0;
            btnAddNewWidgets.FlatStyle = FlatStyle.Flat;
            btnAddNewWidgets.Font = new Font("Segoe UI Symbol", 10.2F, FontStyle.Regular, GraphicsUnit.Point);
            btnAddNewWidgets.Image = (Image)resources.GetObject("btnAddNewWidgets.Image");
            btnAddNewWidgets.ImageAlign = ContentAlignment.MiddleLeft;
            btnAddNewWidgets.Location = new Point(3, 53);
            btnAddNewWidgets.Name = "btnAddNewWidgets";
            btnAddNewWidgets.Size = new Size(204, 30);
            btnAddNewWidgets.TabIndex = 4;
            btnAddNewWidgets.Text = "    Add New Widgets";
            btnAddNewWidgets.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Tahoma", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label2.Location = new Point(1150, 5);
            label2.Name = "label2";
            label2.Size = new Size(68, 21);
            label2.TabIndex = 2;
            label2.Text = "Search";
            // 
            // textboxsearch
            // 
            textboxsearch.Location = new Point(1228, 3);
            textboxsearch.Name = "textboxsearch";
            textboxsearch.Size = new Size(210, 27);
            textboxsearch.TabIndex = 1;
            // 
            // panel12
            // 
            panel12.Anchor = AnchorStyles.Top;
            panel12.Controls.Add(panel5);
            panel12.Controls.Add(panel2);
            panel12.Controls.Add(panel4);
            panel12.Location = new Point(1097, 95);
            panel12.Name = "panel12";
            panel12.Size = new Size(344, 449);
            panel12.TabIndex = 3;
            // 
            // panel5
            // 
            panel5.BackColor = Color.Black;
            panel5.Controls.Add(PercentTotalRevenue);
            panel5.Controls.Add(pictureBoxTotalRevenue);
            panel5.Controls.Add(CounterTotalRevenue);
            panel5.Controls.Add(pictureBox6);
            panel5.Controls.Add(label13);
            panel5.Font = new Font("Segoe UI", 10.2F, FontStyle.Regular, GraphicsUnit.Point);
            panel5.Location = new Point(3, 304);
            panel5.Name = "panel5";
            panel5.Size = new Size(332, 142);
            panel5.TabIndex = 6;
            // 
            // PercentTotalRevenue
            // 
            PercentTotalRevenue.AutoSize = true;
            PercentTotalRevenue.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            PercentTotalRevenue.ForeColor = Color.White;
            PercentTotalRevenue.Location = new Point(124, 74);
            PercentTotalRevenue.Name = "PercentTotalRevenue";
            PercentTotalRevenue.Size = new Size(34, 20);
            PercentTotalRevenue.TabIndex = 4;
            PercentTotalRevenue.Text = "-0.5";
            // 
            // pictureBoxTotalRevenue
            // 
            pictureBoxTotalRevenue.Image = (Image)resources.GetObject("pictureBoxTotalRevenue.Image");
            pictureBoxTotalRevenue.Location = new Point(87, 66);
            pictureBoxTotalRevenue.Name = "pictureBoxTotalRevenue";
            pictureBoxTotalRevenue.Size = new Size(31, 28);
            pictureBoxTotalRevenue.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBoxTotalRevenue.TabIndex = 3;
            pictureBoxTotalRevenue.TabStop = false;
            // 
            // CounterTotalRevenue
            // 
            CounterTotalRevenue.AutoSize = true;
            CounterTotalRevenue.Font = new Font("Segoe UI", 18F, FontStyle.Regular, GraphicsUnit.Point);
            CounterTotalRevenue.ForeColor = Color.White;
            CounterTotalRevenue.Location = new Point(14, 38);
            CounterTotalRevenue.Name = "CounterTotalRevenue";
            CounterTotalRevenue.Size = new Size(77, 41);
            CounterTotalRevenue.TabIndex = 2;
            CounterTotalRevenue.Text = "$2M";
            // 
            // pictureBox6
            // 
            pictureBox6.Image = (Image)resources.GetObject("pictureBox6.Image");
            pictureBox6.Location = new Point(195, 6);
            pictureBox6.Name = "pictureBox6";
            pictureBox6.Size = new Size(123, 104);
            pictureBox6.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox6.TabIndex = 1;
            pictureBox6.TabStop = false;
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.ForeColor = Color.White;
            label13.Location = new Point(14, 6);
            label13.Name = "label13";
            label13.Size = new Size(116, 23);
            label13.TabIndex = 0;
            label13.Text = "Total Revenue";
            // 
            // panel2
            // 
            panel2.BackColor = Color.FromArgb(0, 104, 255);
            panel2.Controls.Add(PercentWastingList);
            panel2.Controls.Add(pictureBoxWastingList);
            panel2.Controls.Add(CounterWastingList);
            panel2.Controls.Add(pictureBox1);
            panel2.Controls.Add(label5);
            panel2.Font = new Font("Segoe UI", 10.2F, FontStyle.Regular, GraphicsUnit.Point);
            panel2.Location = new Point(3, 3);
            panel2.Name = "panel2";
            panel2.Size = new Size(332, 140);
            panel2.TabIndex = 5;
            // 
            // PercentWastingList
            // 
            PercentWastingList.AutoSize = true;
            PercentWastingList.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            PercentWastingList.ForeColor = Color.White;
            PercentWastingList.Location = new Point(107, 76);
            PercentWastingList.Name = "PercentWastingList";
            PercentWastingList.Size = new Size(38, 20);
            PercentWastingList.TabIndex = 4;
            PercentWastingList.Text = "+1.5";
            // 
            // pictureBoxWastingList
            // 
            pictureBoxWastingList.Image = (Image)resources.GetObject("pictureBoxWastingList.Image");
            pictureBoxWastingList.Location = new Point(78, 68);
            pictureBoxWastingList.Name = "pictureBoxWastingList";
            pictureBoxWastingList.Size = new Size(31, 28);
            pictureBoxWastingList.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBoxWastingList.TabIndex = 3;
            pictureBoxWastingList.TabStop = false;
            // 
            // CounterWastingList
            // 
            CounterWastingList.AutoSize = true;
            CounterWastingList.Font = new Font("Segoe UI", 18F, FontStyle.Regular, GraphicsUnit.Point);
            CounterWastingList.ForeColor = Color.White;
            CounterWastingList.Location = new Point(8, 36);
            CounterWastingList.Name = "CounterWastingList";
            CounterWastingList.Size = new Size(66, 41);
            CounterWastingList.TabIndex = 2;
            CounterWastingList.Text = "840";
            // 
            // pictureBox1
            // 
            pictureBox1.Image = Properties.Resources.wasting_list;
            pictureBox1.Location = new Point(186, 9);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new Size(132, 104);
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox1.TabIndex = 1;
            pictureBox1.TabStop = false;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.ForeColor = Color.White;
            label5.Location = new Point(8, 10);
            label5.Name = "label5";
            label5.Size = new Size(101, 23);
            label5.TabIndex = 0;
            label5.Text = "Wasting List";
            // 
            // panel4
            // 
            panel4.BackColor = Color.FromArgb(201, 123, 243);
            panel4.Controls.Add(PercentCompletedFlights);
            panel4.Controls.Add(pictureBoxCompletedFlights);
            panel4.Controls.Add(CounterCompletedFlights);
            panel4.Controls.Add(pictureBox4);
            panel4.Controls.Add(label10);
            panel4.Font = new Font("Segoe UI", 10.2F, FontStyle.Regular, GraphicsUnit.Point);
            panel4.Location = new Point(3, 158);
            panel4.Name = "panel4";
            panel4.Size = new Size(332, 130);
            panel4.TabIndex = 6;
            // 
            // PercentCompletedFlights
            // 
            PercentCompletedFlights.AutoSize = true;
            PercentCompletedFlights.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            PercentCompletedFlights.ForeColor = Color.White;
            PercentCompletedFlights.Location = new Point(115, 79);
            PercentCompletedFlights.Name = "PercentCompletedFlights";
            PercentCompletedFlights.Size = new Size(38, 20);
            PercentCompletedFlights.TabIndex = 4;
            PercentCompletedFlights.Text = "+0.2";
            // 
            // pictureBoxCompletedFlights
            // 
            pictureBoxCompletedFlights.Image = (Image)resources.GetObject("pictureBoxCompletedFlights.Image");
            pictureBoxCompletedFlights.Location = new Point(78, 71);
            pictureBoxCompletedFlights.Name = "pictureBoxCompletedFlights";
            pictureBoxCompletedFlights.Size = new Size(31, 28);
            pictureBoxCompletedFlights.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBoxCompletedFlights.TabIndex = 3;
            pictureBoxCompletedFlights.TabStop = false;
            // 
            // CounterCompletedFlights
            // 
            CounterCompletedFlights.AutoSize = true;
            CounterCompletedFlights.Font = new Font("Segoe UI", 18F, FontStyle.Regular, GraphicsUnit.Point);
            CounterCompletedFlights.ForeColor = Color.White;
            CounterCompletedFlights.Location = new Point(14, 39);
            CounterCompletedFlights.Name = "CounterCompletedFlights";
            CounterCompletedFlights.Size = new Size(66, 41);
            CounterCompletedFlights.TabIndex = 2;
            CounterCompletedFlights.Text = "235";
            // 
            // pictureBox4
            // 
            pictureBox4.Image = (Image)resources.GetObject("pictureBox4.Image");
            pictureBox4.Location = new Point(186, 10);
            pictureBox4.Name = "pictureBox4";
            pictureBox4.Size = new Size(132, 104);
            pictureBox4.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox4.TabIndex = 1;
            pictureBox4.TabStop = false;
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.ForeColor = Color.White;
            label10.Location = new Point(8, 10);
            label10.Name = "label10";
            label10.Size = new Size(148, 23);
            label10.TabIndex = 0;
            label10.Text = "Completed Flights";
            // 
            // panel3
            // 
            panel3.Anchor = AnchorStyles.Top;
            panel3.Controls.Add(panel8);
            panel3.Controls.Add(panel7);
            panel3.Controls.Add(label4);
            panel3.Controls.Add(btnExport);
            panel3.Controls.Add(label3);
            panel3.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            panel3.Location = new Point(0, 95);
            panel3.Name = "panel3";
            panel3.Size = new Size(1084, 452);
            panel3.TabIndex = 3;
            // 
            // panel8
            // 
            panel8.Controls.Add(panel22);
            panel8.Controls.Add(panel21);
            panel8.Controls.Add(pictureBox8);
            panel8.Controls.Add(pictureBox7);
            panel8.Location = new Point(179, 342);
            panel8.Name = "panel8";
            panel8.Size = new Size(743, 104);
            panel8.TabIndex = 4;
            // 
            // panel22
            // 
            panel22.Controls.Add(label32);
            panel22.Controls.Add(CounterSales);
            panel22.Location = new Point(536, 22);
            panel22.Name = "panel22";
            panel22.Size = new Size(111, 72);
            panel22.TabIndex = 6;
            // 
            // label32
            // 
            label32.AutoSize = true;
            label32.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label32.Location = new Point(3, 0);
            label32.Name = "label32";
            label32.Size = new Size(44, 20);
            label32.TabIndex = 2;
            label32.Text = "Sales";
            // 
            // CounterSales
            // 
            CounterSales.AutoSize = true;
            CounterSales.Font = new Font("Segoe UI", 16.2F, FontStyle.Regular, GraphicsUnit.Point);
            CounterSales.ForeColor = Color.Black;
            CounterSales.Location = new Point(3, 28);
            CounterSales.Name = "CounterSales";
            CounterSales.Size = new Size(83, 38);
            CounterSales.TabIndex = 5;
            CounterSales.Text = "2,564";
            // 
            // panel21
            // 
            panel21.Controls.Add(label15);
            panel21.Controls.Add(CounterViews);
            panel21.Location = new Point(97, 22);
            panel21.Name = "panel21";
            panel21.Size = new Size(111, 72);
            panel21.TabIndex = 0;
            // 
            // label15
            // 
            label15.AutoSize = true;
            label15.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label15.Location = new Point(3, 0);
            label15.Name = "label15";
            label15.Size = new Size(50, 20);
            label15.TabIndex = 2;
            label15.Text = "Views";
            // 
            // CounterViews
            // 
            CounterViews.AutoSize = true;
            CounterViews.Font = new Font("Segoe UI", 16.2F, FontStyle.Regular, GraphicsUnit.Point);
            CounterViews.ForeColor = Color.Black;
            CounterViews.Location = new Point(3, 28);
            CounterViews.Name = "CounterViews";
            CounterViews.Size = new Size(98, 38);
            CounterViews.TabIndex = 5;
            CounterViews.Text = "20,751";
            // 
            // pictureBox8
            // 
            pictureBox8.Image = (Image)resources.GetObject("pictureBox8.Image");
            pictureBox8.Location = new Point(434, 22);
            pictureBox8.Name = "pictureBox8";
            pictureBox8.Size = new Size(67, 71);
            pictureBox8.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox8.TabIndex = 1;
            pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            pictureBox7.Image = Properties.Resources.icons8_chart_50;
            pictureBox7.Location = new Point(9, 22);
            pictureBox7.Name = "pictureBox7";
            pictureBox7.Size = new Size(67, 71);
            pictureBox7.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox7.TabIndex = 0;
            pictureBox7.TabStop = false;
            // 
            // panel7
            // 
            panel7.Location = new Point(13, 59);
            panel7.Name = "panel7";
            panel7.Size = new Size(1047, 274);
            panel7.TabIndex = 3;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Segoe UI", 7.20000029F, FontStyle.Bold, GraphicsUnit.Point);
            label4.Location = new Point(3, 36);
            label4.Name = "label4";
            label4.Size = new Size(176, 17);
            label4.TabIndex = 2;
            label4.Text = "Visited 10 of 100 countries.";
            // 
            // btnExport
            // 
            btnExport.ForeColor = SystemColors.ActiveBorder;
            btnExport.Image = Properties.Resources.export;
            btnExport.ImageAlign = ContentAlignment.MiddleLeft;
            btnExport.Location = new Point(964, 3);
            btnExport.Name = "btnExport";
            btnExport.Size = new Size(111, 37);
            btnExport.TabIndex = 1;
            btnExport.Text = "    Export";
            btnExport.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Tahoma", 12F, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point);
            label3.Location = new Point(0, 3);
            label3.Name = "label3";
            label3.Size = new Size(174, 24);
            label3.TabIndex = 0;
            label3.Text = "Sale Performent";
            // 
            // panel6
            // 
            panel6.Anchor = AnchorStyles.Top;
            panel6.Controls.Add(panel11);
            panel6.Controls.Add(panel10);
            panel6.Controls.Add(panel9);
            panel6.Location = new Point(2, 556);
            panel6.Name = "panel6";
            panel6.Size = new Size(1441, 351);
            panel6.TabIndex = 2;
            // 
            // panel11
            // 
            panel11.Controls.Add(label21);
            panel11.Controls.Add(label20);
            panel11.Controls.Add(label19);
            panel11.Location = new Point(3, 91);
            panel11.Name = "panel11";
            panel11.Size = new Size(1435, 45);
            panel11.TabIndex = 13;
            // 
            // label21
            // 
            label21.AutoSize = true;
            label21.Font = new Font("Segoe UI", 7.20000029F, FontStyle.Bold, GraphicsUnit.Point);
            label21.Location = new Point(1362, 18);
            label21.Name = "label21";
            label21.Size = new Size(50, 17);
            label21.TabIndex = 8;
            label21.Text = "People";
            // 
            // label20
            // 
            label20.AutoSize = true;
            label20.Font = new Font("Segoe UI", 7.20000029F, FontStyle.Bold, GraphicsUnit.Point);
            label20.Location = new Point(1071, 18);
            label20.Name = "label20";
            label20.Size = new Size(37, 17);
            label20.TabIndex = 7;
            label20.Text = "Date";
            // 
            // label19
            // 
            label19.AutoSize = true;
            label19.Font = new Font("Segoe UI", 7.20000029F, FontStyle.Bold, GraphicsUnit.Point);
            label19.Location = new Point(9, 18);
            label19.Name = "label19";
            label19.Size = new Size(80, 17);
            label19.TabIndex = 5;
            label19.Text = "Destination\r\n";
            // 
            // panel10
            // 
            panel10.Controls.Add(label14);
            panel10.Controls.Add(DepartureDate);
            panel10.Controls.Add(dateTimePicker1);
            panel10.Controls.Add(BookingTypes);
            panel10.Controls.Add(DateRange);
            panel10.Location = new Point(3, 3);
            panel10.Name = "panel10";
            panel10.Size = new Size(1433, 76);
            panel10.TabIndex = 12;
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Font = new Font("Tahoma", 12F, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point);
            label14.Location = new Point(3, 0);
            label14.Name = "label14";
            label14.Size = new Size(174, 24);
            label14.TabIndex = 3;
            label14.Text = "Sale Performent";
            // 
            // DepartureDate
            // 
            DepartureDate.FormattingEnabled = true;
            DepartureDate.Location = new Point(8, 45);
            DepartureDate.Name = "DepartureDate";
            DepartureDate.Size = new Size(151, 28);
            DepartureDate.TabIndex = 4;
            // 
            // dateTimePicker1
            // 
            dateTimePicker1.Location = new Point(1310, 3);
            dateTimePicker1.Name = "dateTimePicker1";
            dateTimePicker1.Size = new Size(119, 27);
            dateTimePicker1.TabIndex = 10;
            // 
            // BookingTypes
            // 
            BookingTypes.FormattingEnabled = true;
            BookingTypes.Location = new Point(253, 45);
            BookingTypes.Name = "BookingTypes";
            BookingTypes.Size = new Size(151, 28);
            BookingTypes.TabIndex = 5;
            // 
            // DateRange
            // 
            DateRange.FormattingEnabled = true;
            DateRange.Location = new Point(505, 45);
            DateRange.Name = "DateRange";
            DateRange.Size = new Size(151, 28);
            DateRange.TabIndex = 6;
            // 
            // panel9
            // 
            panel9.Controls.Add(panel20);
            panel9.Controls.Add(panel19);
            panel9.Controls.Add(panel18);
            panel9.Controls.Add(panel17);
            panel9.Controls.Add(panel16);
            panel9.Controls.Add(panel15);
            panel9.Controls.Add(panel14);
            panel9.Location = new Point(3, 142);
            panel9.Name = "panel9";
            panel9.Size = new Size(1433, 188);
            panel9.TabIndex = 11;
            // 
            // panel20
            // 
            panel20.Controls.Add(CounterPeopleFlight1);
            panel20.Controls.Add(CounterPeopleFlight2);
            panel20.Location = new Point(1362, 9);
            panel20.Name = "panel20";
            panel20.Size = new Size(59, 160);
            panel20.TabIndex = 33;
            // 
            // CounterPeopleFlight1
            // 
            CounterPeopleFlight1.AutoSize = true;
            CounterPeopleFlight1.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            CounterPeopleFlight1.ForeColor = Color.Black;
            CounterPeopleFlight1.Location = new Point(15, 18);
            CounterPeopleFlight1.Name = "CounterPeopleFlight1";
            CounterPeopleFlight1.Size = new Size(32, 25);
            CounterPeopleFlight1.TabIndex = 29;
            CounterPeopleFlight1.Text = "50";
            // 
            // CounterPeopleFlight2
            // 
            CounterPeopleFlight2.AutoSize = true;
            CounterPeopleFlight2.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            CounterPeopleFlight2.ForeColor = Color.Black;
            CounterPeopleFlight2.Location = new Point(15, 111);
            CounterPeopleFlight2.Name = "CounterPeopleFlight2";
            CounterPeopleFlight2.Size = new Size(32, 25);
            CounterPeopleFlight2.TabIndex = 30;
            CounterPeopleFlight2.Text = "20";
            // 
            // panel19
            // 
            panel19.Controls.Add(pictureBox14);
            panel19.Controls.Add(DestinationSymbolFlight2);
            panel19.Controls.Add(label33);
            panel19.Controls.Add(DepartureSympolCountry2);
            panel19.Controls.Add(label42);
            panel19.Controls.Add(pictureBox15);
            panel19.Controls.Add(pictureBox16);
            panel19.Location = new Point(241, 100);
            panel19.Name = "panel19";
            panel19.Size = new Size(423, 69);
            panel19.TabIndex = 17;
            // 
            // pictureBox14
            // 
            pictureBox14.Image = (Image)resources.GetObject("pictureBox14.Image");
            pictureBox14.Location = new Point(41, 10);
            pictureBox14.Name = "pictureBox14";
            pictureBox14.Size = new Size(31, 35);
            pictureBox14.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox14.TabIndex = 11;
            pictureBox14.TabStop = false;
            // 
            // DestinationSymbolFlight2
            // 
            DestinationSymbolFlight2.AutoSize = true;
            DestinationSymbolFlight2.Font = new Font("Segoe UI", 7.20000029F, FontStyle.Bold, GraphicsUnit.Point);
            DestinationSymbolFlight2.Location = new Point(343, 48);
            DestinationSymbolFlight2.Name = "DestinationSymbolFlight2";
            DestinationSymbolFlight2.Size = new Size(37, 17);
            DestinationSymbolFlight2.TabIndex = 21;
            DestinationSymbolFlight2.Text = "DME";
            // 
            // label33
            // 
            label33.AutoSize = true;
            label33.Font = new Font("Segoe UI", 7.20000029F, FontStyle.Bold, GraphicsUnit.Point);
            label33.Location = new Point(136, 0);
            label33.Name = "label33";
            label33.Size = new Size(67, 17);
            label33.TabIndex = 15;
            label33.Text = "Duration:";
            // 
            // DepartureSympolCountry2
            // 
            DepartureSympolCountry2.AutoSize = true;
            DepartureSympolCountry2.Font = new Font("Segoe UI", 7.20000029F, FontStyle.Bold, GraphicsUnit.Point);
            DepartureSympolCountry2.Location = new Point(41, 48);
            DepartureSympolCountry2.Name = "DepartureSympolCountry2";
            DepartureSympolCountry2.Size = new Size(33, 17);
            DepartureSympolCountry2.TabIndex = 18;
            DepartureSympolCountry2.Text = "LHR";
            // 
            // label42
            // 
            label42.AutoSize = true;
            label42.Font = new Font("Segoe UI", 7.20000029F, FontStyle.Bold, GraphicsUnit.Point);
            label42.Location = new Point(209, 0);
            label42.Name = "label42";
            label42.Size = new Size(53, 17);
            label42.TabIndex = 16;
            label42.Text = "2h 30m";
            // 
            // pictureBox15
            // 
            pictureBox15.Image = (Image)resources.GetObject("pictureBox15.Image");
            pictureBox15.Location = new Point(66, 20);
            pictureBox15.Name = "pictureBox15";
            pictureBox15.Size = new Size(278, 25);
            pictureBox15.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox15.TabIndex = 13;
            pictureBox15.TabStop = false;
            // 
            // pictureBox16
            // 
            pictureBox16.Image = (Image)resources.GetObject("pictureBox16.Image");
            pictureBox16.Location = new Point(343, 10);
            pictureBox16.Name = "pictureBox16";
            pictureBox16.Size = new Size(31, 35);
            pictureBox16.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox16.TabIndex = 12;
            pictureBox16.TabStop = false;
            // 
            // panel18
            // 
            panel18.Controls.Add(pictureBox11);
            panel18.Controls.Add(DepartureSympolCountry1);
            panel18.Controls.Add(label28);
            panel18.Controls.Add(label29);
            panel18.Controls.Add(pictureBox13);
            panel18.Controls.Add(pictureBox12);
            panel18.Controls.Add(DestinationSymbolFlight1);
            panel18.Location = new Point(241, 20);
            panel18.Name = "panel18";
            panel18.Size = new Size(423, 69);
            panel18.TabIndex = 0;
            // 
            // pictureBox11
            // 
            pictureBox11.Image = (Image)resources.GetObject("pictureBox11.Image");
            pictureBox11.Location = new Point(41, 10);
            pictureBox11.Name = "pictureBox11";
            pictureBox11.Size = new Size(31, 35);
            pictureBox11.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox11.TabIndex = 11;
            pictureBox11.TabStop = false;
            // 
            // DepartureSympolCountry1
            // 
            DepartureSympolCountry1.AutoSize = true;
            DepartureSympolCountry1.Font = new Font("Segoe UI", 7.20000029F, FontStyle.Bold, GraphicsUnit.Point);
            DepartureSympolCountry1.Location = new Point(41, 48);
            DepartureSympolCountry1.Name = "DepartureSympolCountry1";
            DepartureSympolCountry1.Size = new Size(29, 17);
            DepartureSympolCountry1.TabIndex = 9;
            DepartureSympolCountry1.Text = "SIN";
            // 
            // label28
            // 
            label28.AutoSize = true;
            label28.Font = new Font("Segoe UI", 7.20000029F, FontStyle.Bold, GraphicsUnit.Point);
            label28.Location = new Point(136, 0);
            label28.Name = "label28";
            label28.Size = new Size(67, 17);
            label28.TabIndex = 15;
            label28.Text = "Duration:";
            // 
            // label29
            // 
            label29.AutoSize = true;
            label29.Font = new Font("Segoe UI", 7.20000029F, FontStyle.Bold, GraphicsUnit.Point);
            label29.Location = new Point(209, 0);
            label29.Name = "label29";
            label29.Size = new Size(53, 17);
            label29.TabIndex = 16;
            label29.Text = "2h 30m";
            // 
            // pictureBox13
            // 
            pictureBox13.Image = (Image)resources.GetObject("pictureBox13.Image");
            pictureBox13.Location = new Point(66, 20);
            pictureBox13.Name = "pictureBox13";
            pictureBox13.Size = new Size(278, 25);
            pictureBox13.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox13.TabIndex = 13;
            pictureBox13.TabStop = false;
            // 
            // pictureBox12
            // 
            pictureBox12.Image = (Image)resources.GetObject("pictureBox12.Image");
            pictureBox12.Location = new Point(343, 10);
            pictureBox12.Name = "pictureBox12";
            pictureBox12.Size = new Size(31, 35);
            pictureBox12.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox12.TabIndex = 12;
            pictureBox12.TabStop = false;
            // 
            // DestinationSymbolFlight1
            // 
            DestinationSymbolFlight1.AutoSize = true;
            DestinationSymbolFlight1.Font = new Font("Segoe UI", 7.20000029F, FontStyle.Bold, GraphicsUnit.Point);
            DestinationSymbolFlight1.Location = new Point(343, 45);
            DestinationSymbolFlight1.Name = "DestinationSymbolFlight1";
            DestinationSymbolFlight1.Size = new Size(36, 17);
            DestinationSymbolFlight1.TabIndex = 14;
            DestinationSymbolFlight1.Text = "BDO";
            // 
            // panel17
            // 
            panel17.Controls.Add(TimeFlightDestination1);
            panel17.Controls.Add(CountryFlightDestination1);
            panel17.Controls.Add(TimeFlightDestination2);
            panel17.Controls.Add(CountryFlightDestination2);
            panel17.Location = new Point(772, 9);
            panel17.Name = "panel17";
            panel17.Size = new Size(170, 160);
            panel17.TabIndex = 32;
            // 
            // TimeFlightDestination1
            // 
            TimeFlightDestination1.AutoSize = true;
            TimeFlightDestination1.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            TimeFlightDestination1.ForeColor = Color.Black;
            TimeFlightDestination1.Location = new Point(53, 15);
            TimeFlightDestination1.Name = "TimeFlightDestination1";
            TimeFlightDestination1.Size = new Size(60, 28);
            TimeFlightDestination1.TabIndex = 10;
            TimeFlightDestination1.Text = "06:10";
            // 
            // CountryFlightDestination1
            // 
            CountryFlightDestination1.AutoSize = true;
            CountryFlightDestination1.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            CountryFlightDestination1.ForeColor = Color.Black;
            CountryFlightDestination1.Location = new Point(48, 43);
            CountryFlightDestination1.Name = "CountryFlightDestination1";
            CountryFlightDestination1.Size = new Size(85, 25);
            CountryFlightDestination1.TabIndex = 10;
            CountryFlightDestination1.Text = "BanDung";
            // 
            // TimeFlightDestination2
            // 
            TimeFlightDestination2.AutoSize = true;
            TimeFlightDestination2.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            TimeFlightDestination2.ForeColor = Color.Black;
            TimeFlightDestination2.Location = new Point(53, 93);
            TimeFlightDestination2.Name = "TimeFlightDestination2";
            TimeFlightDestination2.Size = new Size(60, 28);
            TimeFlightDestination2.TabIndex = 24;
            TimeFlightDestination2.Text = "23:30";
            // 
            // CountryFlightDestination2
            // 
            CountryFlightDestination2.AutoSize = true;
            CountryFlightDestination2.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            CountryFlightDestination2.ForeColor = Color.Black;
            CountryFlightDestination2.Location = new Point(48, 129);
            CountryFlightDestination2.Name = "CountryFlightDestination2";
            CountryFlightDestination2.Size = new Size(77, 25);
            CountryFlightDestination2.TabIndex = 25;
            CountryFlightDestination2.Text = "Mascow";
            // 
            // panel16
            // 
            panel16.Controls.Add(DateFlights1);
            panel16.Controls.Add(DateFlights2);
            panel16.Location = new Point(1039, 9);
            panel16.Name = "panel16";
            panel16.Size = new Size(120, 160);
            panel16.TabIndex = 31;
            // 
            // DateFlights1
            // 
            DateFlights1.AutoSize = true;
            DateFlights1.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            DateFlights1.ForeColor = Color.Black;
            DateFlights1.Location = new Point(5, 18);
            DateFlights1.Name = "DateFlights1";
            DateFlights1.Size = new Size(106, 25);
            DateFlights1.TabIndex = 26;
            DateFlights1.Text = "12/12/2012";
            // 
            // DateFlights2
            // 
            DateFlights2.AutoSize = true;
            DateFlights2.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            DateFlights2.ForeColor = Color.Black;
            DateFlights2.Location = new Point(5, 111);
            DateFlights2.Name = "DateFlights2";
            DateFlights2.Size = new Size(106, 25);
            DateFlights2.TabIndex = 28;
            DateFlights2.Text = "12/12/2012";
            // 
            // panel15
            // 
            panel15.Controls.Add(CountryFlightDeparture2);
            panel15.Controls.Add(TimeFlightDeparture2);
            panel15.Controls.Add(pictureBoxDestinationFlight2);
            panel15.Location = new Point(8, 100);
            panel15.Name = "panel15";
            panel15.Size = new Size(194, 69);
            panel15.TabIndex = 10;
            // 
            // CountryFlightDeparture2
            // 
            CountryFlightDeparture2.AutoSize = true;
            CountryFlightDeparture2.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            CountryFlightDeparture2.ForeColor = Color.Black;
            CountryFlightDeparture2.Location = new Point(80, 34);
            CountryFlightDeparture2.Name = "CountryFlightDeparture2";
            CountryFlightDeparture2.Size = new Size(73, 25);
            CountryFlightDeparture2.TabIndex = 9;
            CountryFlightDeparture2.Text = "London";
            // 
            // TimeFlightDeparture2
            // 
            TimeFlightDeparture2.AutoSize = true;
            TimeFlightDeparture2.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            TimeFlightDeparture2.ForeColor = Color.Black;
            TimeFlightDeparture2.Location = new Point(80, 6);
            TimeFlightDeparture2.Name = "TimeFlightDeparture2";
            TimeFlightDeparture2.Size = new Size(60, 28);
            TimeFlightDeparture2.TabIndex = 8;
            TimeFlightDeparture2.Text = "20:00";
            // 
            // pictureBoxDestinationFlight2
            // 
            pictureBoxDestinationFlight2.Image = (Image)resources.GetObject("pictureBoxDestinationFlight2.Image");
            pictureBoxDestinationFlight2.Location = new Point(6, 6);
            pictureBoxDestinationFlight2.Name = "pictureBoxDestinationFlight2";
            pictureBoxDestinationFlight2.Size = new Size(62, 57);
            pictureBoxDestinationFlight2.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBoxDestinationFlight2.TabIndex = 0;
            pictureBoxDestinationFlight2.TabStop = false;
            // 
            // panel14
            // 
            panel14.Controls.Add(CountryFlightDeparture1);
            panel14.Controls.Add(TimeFlightDeparture1);
            panel14.Controls.Add(pictureBoxDestinationFLight1);
            panel14.Location = new Point(8, 15);
            panel14.Name = "panel14";
            panel14.Size = new Size(194, 74);
            panel14.TabIndex = 0;
            // 
            // CountryFlightDeparture1
            // 
            CountryFlightDeparture1.AutoSize = true;
            CountryFlightDeparture1.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            CountryFlightDeparture1.ForeColor = Color.Black;
            CountryFlightDeparture1.Location = new Point(80, 34);
            CountryFlightDeparture1.Name = "CountryFlightDeparture1";
            CountryFlightDeparture1.Size = new Size(93, 25);
            CountryFlightDeparture1.TabIndex = 9;
            CountryFlightDeparture1.Text = "Singapore";
            // 
            // TimeFlightDeparture1
            // 
            TimeFlightDeparture1.AutoSize = true;
            TimeFlightDeparture1.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            TimeFlightDeparture1.ForeColor = Color.Black;
            TimeFlightDeparture1.Location = new Point(80, 6);
            TimeFlightDeparture1.Name = "TimeFlightDeparture1";
            TimeFlightDeparture1.Size = new Size(60, 28);
            TimeFlightDeparture1.TabIndex = 8;
            TimeFlightDeparture1.Text = "06:10";
            // 
            // pictureBoxDestinationFLight1
            // 
            pictureBoxDestinationFLight1.Image = (Image)resources.GetObject("pictureBoxDestinationFLight1.Image");
            pictureBoxDestinationFLight1.Location = new Point(6, 6);
            pictureBoxDestinationFLight1.Name = "pictureBoxDestinationFLight1";
            pictureBoxDestinationFLight1.Size = new Size(62, 62);
            pictureBoxDestinationFLight1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBoxDestinationFLight1.TabIndex = 0;
            pictureBoxDestinationFLight1.TabStop = false;
            // 
            // Form_DashBoard
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1455, 910);
            Controls.Add(panel6);
            Controls.Add(panel1);
            Name = "Form_DashBoard";
            Text = "Form_DashBoard";
            panel1.ResumeLayout(false);
            panel13.ResumeLayout(false);
            panel13.PerformLayout();
            panel12.ResumeLayout(false);
            panel5.ResumeLayout(false);
            panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBoxTotalRevenue).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox6).EndInit();
            panel2.ResumeLayout(false);
            panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBoxWastingList).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            panel4.ResumeLayout(false);
            panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBoxCompletedFlights).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox4).EndInit();
            panel3.ResumeLayout(false);
            panel3.PerformLayout();
            panel8.ResumeLayout(false);
            panel22.ResumeLayout(false);
            panel22.PerformLayout();
            panel21.ResumeLayout(false);
            panel21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox8).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox7).EndInit();
            panel6.ResumeLayout(false);
            panel11.ResumeLayout(false);
            panel11.PerformLayout();
            panel10.ResumeLayout(false);
            panel10.PerformLayout();
            panel9.ResumeLayout(false);
            panel20.ResumeLayout(false);
            panel20.PerformLayout();
            panel19.ResumeLayout(false);
            panel19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox14).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox15).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox16).EndInit();
            panel18.ResumeLayout(false);
            panel18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox11).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox13).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox12).EndInit();
            panel17.ResumeLayout(false);
            panel17.PerformLayout();
            panel16.ResumeLayout(false);
            panel16.PerformLayout();
            panel15.ResumeLayout(false);
            panel15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBoxDestinationFlight2).EndInit();
            panel14.ResumeLayout(false);
            panel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBoxDestinationFLight1).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private Label label1;
        private Panel panel1;
        private TextBox textboxsearch;
        private Label label2;
        private Panel panel3;
        private Label label3;
        private Button btnExport;
        private Button btnAddNewWidgets;
        private Label label4;
        private Panel panel2;
        private Label label5;
        private PictureBox pictureBox1;
        private Label CounterWastingList;
        private Label PercentWastingList;
        private PictureBox pictureBoxWastingList;
        private Panel panel5;
        private Label PercentTotalRevenue;
        private PictureBox pictureBoxTotalRevenue;
        private Label CounterTotalRevenue;
        private PictureBox pictureBox6;
        private Label label13;
        private Panel panel4;
        private Label PercentCompletedFlights;
        private PictureBox pictureBoxCompletedFlights;
        private Label CounterCompletedFlights;
        private PictureBox pictureBox4;
        private Label label10;
        private Panel panel8;
        private Panel panel7;
        private Panel panel6;
        private Label label14;
        private PictureBox pictureBox7;
        private Label CounterViews;
        private Label label15;
        private PictureBox pictureBox8;
        private DateTimePicker dateTimePicker1;
        private Label label21;
        private Label label20;
        private Label label19;
        private ComboBox DateRange;
        private ComboBox BookingTypes;
        private ComboBox DepartureDate;
        private Panel panel11;
        private Panel panel10;
        private Panel panel9;
        private Panel panel13;
        private Panel panel12;
        private Panel panel14;
        private PictureBox pictureBoxDestinationFLight1;
        private Label CountryFlightDeparture1;
        private Label TimeFlightDeparture1;
        private Panel panel15;
        private Label CountryFlightDeparture2;
        private Label TimeFlightDeparture2;
        private PictureBox pictureBoxDestinationFlight2;
        private PictureBox pictureBox11;
        private PictureBox pictureBox13;
        private PictureBox pictureBox12;
        private Label DestinationSymbolFlight2;
        private Label DepartureSympolCountry2;
        private Label label29;
        private Label label28;
        private Label DestinationSymbolFlight1;
        private Label DepartureSympolCountry1;
        private Label CounterPeopleFlight2;
        private Label CounterPeopleFlight1;
        private Label DateFlights2;
        private Label DateFlights1;
        private Label CountryFlightDestination2;
        private Label TimeFlightDestination2;
        private Label CountryFlightDestination1;
        private Label TimeFlightDestination1;
        private Panel panel16;
        private Panel panel17;
        private Panel panel20;
        private Panel panel19;
        private PictureBox pictureBox14;
        private Label label33;
        private Label label42;
        private PictureBox pictureBox15;
        private PictureBox pictureBox16;
        private Panel panel18;
        private Panel panel22;
        private Label label32;
        private Label CounterSales;
        private Panel panel21;
    }
}