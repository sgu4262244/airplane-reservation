﻿namespace Projectc_
{
    partial class Form_Flight
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Flight));
            panel3 = new Panel();
            label7 = new Label();
            pictureBox1 = new PictureBox();
            lblNameAirline = new Label();
            pictureBox3 = new PictureBox();
            label17 = new Label();
            lblDepartAirport = new Label();
            label16 = new Label();
            foxLabel21 = new ReaLTaiizor.Controls.FoxLabel();
            lblCabin = new ReaLTaiizor.Controls.FoxLabel();
            foxLabel14 = new ReaLTaiizor.Controls.FoxLabel();
            label23 = new Label();
            lblCheckIn = new ReaLTaiizor.Controls.FoxLabel();
            foxLabel13 = new ReaLTaiizor.Controls.FoxLabel();
            lblBaggage = new ReaLTaiizor.Controls.FoxLabel();
            pictureBox11 = new PictureBox();
            label22 = new Label();
            lblTotalFlightTime = new Label();
            pictureBox10 = new PictureBox();
            label21 = new Label();
            label5 = new Label();
            pictureBox6 = new PictureBox();
            lblFinishTime = new Label();
            label20 = new Label();
            lblStartTime = new Label();
            pictureBox5 = new PictureBox();
            lblRate = new Label();
            nightLabel2 = new ReaLTaiizor.Controls.NightLabel();
            lblIdFlight = new Label();
            pictureBox13 = new PictureBox();
            imgHang = new PictureBox();
            listView1 = new ListView();
            rdoQuickest = new RadioButton();
            rdoBest = new RadioButton();
            rdoCheapest = new RadioButton();
            label4 = new Label();
            label3 = new Label();
            label2 = new Label();
            label1 = new Label();
            panel5 = new Panel();
            dtpReturnTime = new DateTimePicker();
            cbbClass = new ComboBox();
            groupBox4 = new GroupBox();
            txtTo = new TextBox();
            groupBox3 = new GroupBox();
            txtFrom = new TextBox();
            groupBox2 = new GroupBox();
            cbbType = new ComboBox();
            groupBox1 = new GroupBox();
            panel2 = new Panel();
            radioButton3 = new RadioButton();
            radioButton2 = new RadioButton();
            radioButton1 = new RadioButton();
            checkedListBox1 = new CheckedListBox();
            comboBox2 = new ComboBox();
            comboBox1 = new ComboBox();
            foxLabel17 = new ReaLTaiizor.Controls.FoxLabel();
            foxLabel16 = new ReaLTaiizor.Controls.FoxLabel();
            foxLabel15 = new ReaLTaiizor.Controls.FoxLabel();
            foxLabel12 = new ReaLTaiizor.Controls.FoxLabel();
            panel7 = new Panel();
            label6 = new Label();
            groupBox5 = new GroupBox();
            dtpDepartTime = new DateTimePicker();
            panel1 = new Panel();
            btnBookNow = new ReaLTaiizor.Controls.MaterialButton();
            panelOnway = new Panel();
            btnSearch = new ReaLTaiizor.Controls.HopeRoundButton();
            panel4 = new Panel();
            label14 = new Label();
            label13 = new Label();
            lblPriceDiscount = new Label();
            ForPeople = new Label();
            label12 = new Label();
            pictureBox2 = new PictureBox();
            lblPeiceChildren = new Label();
            lblPriceAdult = new Label();
            label11 = new Label();
            label10 = new Label();
            label9 = new Label();
            lblOld = new Label();
            label8 = new Label();
            button1 = new Button();
            panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox11).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox10).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox6).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox5).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox13).BeginInit();
            ((System.ComponentModel.ISupportInitialize)imgHang).BeginInit();
            panel5.SuspendLayout();
            groupBox4.SuspendLayout();
            groupBox3.SuspendLayout();
            groupBox2.SuspendLayout();
            groupBox1.SuspendLayout();
            panel2.SuspendLayout();
            panel7.SuspendLayout();
            groupBox5.SuspendLayout();
            panel1.SuspendLayout();
            panelOnway.SuspendLayout();
            panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox2).BeginInit();
            SuspendLayout();
            // 
            // panel3
            // 
            panel3.BackColor = Color.White;
            panel3.Controls.Add(button1);
            panel3.Controls.Add(label7);
            panel3.Controls.Add(pictureBox1);
            panel3.Controls.Add(lblNameAirline);
            panel3.Controls.Add(pictureBox3);
            panel3.Controls.Add(label17);
            panel3.Controls.Add(lblDepartAirport);
            panel3.Controls.Add(label16);
            panel3.Controls.Add(foxLabel21);
            panel3.Controls.Add(lblCabin);
            panel3.Controls.Add(foxLabel14);
            panel3.Controls.Add(label23);
            panel3.Controls.Add(lblCheckIn);
            panel3.Controls.Add(foxLabel13);
            panel3.Controls.Add(lblBaggage);
            panel3.Controls.Add(pictureBox11);
            panel3.Controls.Add(label22);
            panel3.Controls.Add(lblTotalFlightTime);
            panel3.Controls.Add(pictureBox10);
            panel3.Controls.Add(label21);
            panel3.Controls.Add(label5);
            panel3.Controls.Add(pictureBox6);
            panel3.Controls.Add(lblFinishTime);
            panel3.Controls.Add(label20);
            panel3.Controls.Add(lblStartTime);
            panel3.Controls.Add(pictureBox5);
            panel3.Controls.Add(lblRate);
            panel3.Controls.Add(nightLabel2);
            panel3.Controls.Add(lblIdFlight);
            panel3.Controls.Add(pictureBox13);
            panel3.Controls.Add(imgHang);
            panel3.Location = new Point(0, 3);
            panel3.Name = "panel3";
            panel3.Size = new Size(679, 303);
            panel3.TabIndex = 0;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Cursor = Cursors.Hand;
            label7.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label7.Location = new Point(441, 201);
            label7.Name = "label7";
            label7.Size = new Size(168, 20);
            label7.TabIndex = 65;
            label7.Text = "Change flight schedule";
            label7.Click += label7_Click;
            // 
            // pictureBox1
            // 
            pictureBox1.Image = (Image)resources.GetObject("pictureBox1.Image");
            pictureBox1.Location = new Point(403, 197);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new Size(26, 29);
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox1.TabIndex = 64;
            pictureBox1.TabStop = false;
            // 
            // lblNameAirline
            // 
            lblNameAirline.Font = new Font("Segoe UI", 13.8F, FontStyle.Bold, GraphicsUnit.Point);
            lblNameAirline.Location = new Point(114, 59);
            lblNameAirline.Name = "lblNameAirline";
            lblNameAirline.Size = new Size(115, 36);
            lblNameAirline.TabIndex = 63;
            lblNameAirline.Text = "VNE";
            lblNameAirline.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // pictureBox3
            // 
            pictureBox3.Image = (Image)resources.GetObject("pictureBox3.Image");
            pictureBox3.Location = new Point(0, 38);
            pictureBox3.Name = "pictureBox3";
            pictureBox3.Size = new Size(676, 18);
            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox3.TabIndex = 62;
            pictureBox3.TabStop = false;
            // 
            // label17
            // 
            label17.Font = new Font("Segoe UI", 10.8F, FontStyle.Bold, GraphicsUnit.Point);
            label17.Location = new Point(476, 107);
            label17.Name = "label17";
            label17.RightToLeft = RightToLeft.No;
            label17.Size = new Size(57, 25);
            label17.TabIndex = 61;
            label17.Text = "HAN";
            // 
            // lblDepartAirport
            // 
            lblDepartAirport.Font = new Font("Segoe UI", 10.8F, FontStyle.Bold, GraphicsUnit.Point);
            lblDepartAirport.Location = new Point(253, 109);
            lblDepartAirport.Name = "lblDepartAirport";
            lblDepartAirport.RightToLeft = RightToLeft.Yes;
            lblDepartAirport.Size = new Size(57, 25);
            lblDepartAirport.TabIndex = 60;
            lblDepartAirport.Text = "SNG";
            // 
            // label16
            // 
            label16.AutoSize = true;
            label16.Font = new Font("Segoe UI", 13.8F, FontStyle.Bold, GraphicsUnit.Point);
            label16.Location = new Point(176, 5);
            label16.Name = "label16";
            label16.Size = new Size(258, 31);
            label16.TabIndex = 59;
            label16.Text = "FLIGHT INFORMATION";
            // 
            // foxLabel21
            // 
            foxLabel21.Font = new Font("Segoe UI", 10F, FontStyle.Bold, GraphicsUnit.Point);
            foxLabel21.ForeColor = Color.FromArgb(76, 88, 100);
            foxLabel21.Location = new Point(18, 230);
            foxLabel21.Name = "foxLabel21";
            foxLabel21.Size = new Size(70, 24);
            foxLabel21.TabIndex = 53;
            foxLabel21.Text = "Cabin :";
            // 
            // lblCabin
            // 
            lblCabin.Font = new Font("Segoe UI", 10F, FontStyle.Bold, GraphicsUnit.Point);
            lblCabin.ForeColor = Color.Black;
            lblCabin.Location = new Point(117, 230);
            lblCabin.Name = "lblCabin";
            lblCabin.Size = new Size(65, 24);
            lblCabin.TabIndex = 52;
            lblCabin.Text = "40kgs";
            // 
            // foxLabel14
            // 
            foxLabel14.Font = new Font("Segoe UI", 10F, FontStyle.Bold, GraphicsUnit.Point);
            foxLabel14.ForeColor = Color.FromArgb(76, 88, 100);
            foxLabel14.Location = new Point(18, 200);
            foxLabel14.Name = "foxLabel14";
            foxLabel14.Size = new Size(93, 24);
            foxLabel14.TabIndex = 51;
            foxLabel14.Text = "Check in :";
            // 
            // label23
            // 
            label23.AutoSize = true;
            label23.Location = new Point(277, 229);
            label23.Name = "label23";
            label23.Size = new Size(80, 20);
            label23.TabIndex = 58;
            label23.Text = "80 cm seat";
            // 
            // lblCheckIn
            // 
            lblCheckIn.Font = new Font("Segoe UI", 10F, FontStyle.Bold, GraphicsUnit.Point);
            lblCheckIn.ForeColor = Color.Black;
            lblCheckIn.Location = new Point(117, 200);
            lblCheckIn.Name = "lblCheckIn";
            lblCheckIn.Size = new Size(65, 24);
            lblCheckIn.TabIndex = 50;
            lblCheckIn.Text = "40kgs";
            // 
            // foxLabel13
            // 
            foxLabel13.Font = new Font("Segoe UI", 10F, FontStyle.Bold, GraphicsUnit.Point);
            foxLabel13.ForeColor = Color.FromArgb(76, 88, 100);
            foxLabel13.Location = new Point(18, 168);
            foxLabel13.Name = "foxLabel13";
            foxLabel13.Size = new Size(93, 24);
            foxLabel13.TabIndex = 49;
            foxLabel13.Text = "Baggage :";
            // 
            // lblBaggage
            // 
            lblBaggage.Font = new Font("Segoe UI", 10F, FontStyle.Bold, GraphicsUnit.Point);
            lblBaggage.ForeColor = Color.Black;
            lblBaggage.Location = new Point(117, 169);
            lblBaggage.Name = "lblBaggage";
            lblBaggage.Size = new Size(65, 24);
            lblBaggage.TabIndex = 48;
            lblBaggage.Text = "Audult";
            // 
            // pictureBox11
            // 
            pictureBox11.Image = (Image)resources.GetObject("pictureBox11.Image");
            pictureBox11.Location = new Point(239, 225);
            pictureBox11.Name = "pictureBox11";
            pictureBox11.Size = new Size(26, 29);
            pictureBox11.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox11.TabIndex = 57;
            pictureBox11.TabStop = false;
            // 
            // label22
            // 
            label22.AutoSize = true;
            label22.Location = new Point(441, 169);
            label22.Name = "label22";
            label22.Size = new Size(92, 20);
            label22.TabIndex = 56;
            label22.Text = "Power outlet";
            // 
            // lblTotalFlightTime
            // 
            lblTotalFlightTime.AccessibleRole = AccessibleRole.None;
            lblTotalFlightTime.Font = new Font("Segoe UI", 10.8F, FontStyle.Bold, GraphicsUnit.Point);
            lblTotalFlightTime.Location = new Point(325, 56);
            lblTotalFlightTime.Name = "lblTotalFlightTime";
            lblTotalFlightTime.Size = new Size(128, 25);
            lblTotalFlightTime.TabIndex = 24;
            lblTotalFlightTime.Text = "2h 15m";
            lblTotalFlightTime.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // pictureBox10
            // 
            pictureBox10.Image = (Image)resources.GetObject("pictureBox10.Image");
            pictureBox10.Location = new Point(403, 165);
            pictureBox10.Name = "pictureBox10";
            pictureBox10.Size = new Size(26, 29);
            pictureBox10.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox10.TabIndex = 55;
            pictureBox10.TabStop = false;
            // 
            // label21
            // 
            label21.AutoSize = true;
            label21.Location = new Point(277, 198);
            label21.Name = "label21";
            label21.Size = new Size(101, 20);
            label21.TabIndex = 54;
            label21.Text = "Food Avilabel";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new Point(371, 112);
            label5.Name = "label5";
            label5.Size = new Size(38, 20);
            label5.TabIndex = 20;
            label5.Text = "HKG";
            // 
            // pictureBox6
            // 
            pictureBox6.Image = (Image)resources.GetObject("pictureBox6.Image");
            pictureBox6.Location = new Point(239, 194);
            pictureBox6.Name = "pictureBox6";
            pictureBox6.Size = new Size(26, 29);
            pictureBox6.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox6.TabIndex = 53;
            pictureBox6.TabStop = false;
            // 
            // lblFinishTime
            // 
            lblFinishTime.AutoSize = true;
            lblFinishTime.Font = new Font("Segoe UI", 10.8F, FontStyle.Bold, GraphicsUnit.Point);
            lblFinishTime.Location = new Point(476, 85);
            lblFinishTime.Name = "lblFinishTime";
            lblFinishTime.Size = new Size(57, 25);
            lblFinishTime.TabIndex = 19;
            lblFinishTime.Text = "14:45";
            // 
            // label20
            // 
            label20.AutoSize = true;
            label20.Location = new Point(277, 168);
            label20.Name = "label20";
            label20.Size = new Size(38, 20);
            label20.TabIndex = 52;
            label20.Text = "WIFI";
            // 
            // lblStartTime
            // 
            lblStartTime.AutoSize = true;
            lblStartTime.Font = new Font("Segoe UI", 10.8F, FontStyle.Bold, GraphicsUnit.Point);
            lblStartTime.Location = new Point(253, 84);
            lblStartTime.Name = "lblStartTime";
            lblStartTime.Size = new Size(57, 25);
            lblStartTime.TabIndex = 18;
            lblStartTime.Text = "12:30";
            // 
            // pictureBox5
            // 
            pictureBox5.Image = (Image)resources.GetObject("pictureBox5.Image");
            pictureBox5.Location = new Point(239, 164);
            pictureBox5.Name = "pictureBox5";
            pictureBox5.Size = new Size(26, 29);
            pictureBox5.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox5.TabIndex = 51;
            pictureBox5.TabStop = false;
            // 
            // lblRate
            // 
            lblRate.AutoSize = true;
            lblRate.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            lblRate.ForeColor = Color.FromArgb(247, 151, 30);
            lblRate.Location = new Point(73, 13);
            lblRate.Name = "lblRate";
            lblRate.Size = new Size(35, 23);
            lblRate.TabIndex = 17;
            lblRate.Text = "4.5";
            // 
            // nightLabel2
            // 
            nightLabel2.AutoSize = true;
            nightLabel2.BackColor = Color.Transparent;
            nightLabel2.Font = new Font("Segoe UI Semibold", 9F, FontStyle.Bold, GraphicsUnit.Point);
            nightLabel2.ForeColor = Color.FromArgb(114, 118, 127);
            nightLabel2.Location = new Point(16, 15);
            nightLabel2.Name = "nightLabel2";
            nightLabel2.Size = new Size(51, 20);
            nightLabel2.TabIndex = 16;
            nightLabel2.Text = "Rate : ";
            // 
            // lblIdFlight
            // 
            lblIdFlight.Font = new Font("Segoe UI Semibold", 10.8F, FontStyle.Bold, GraphicsUnit.Point);
            lblIdFlight.ForeColor = SystemColors.ControlDark;
            lblIdFlight.Location = new Point(114, 107);
            lblIdFlight.Name = "lblIdFlight";
            lblIdFlight.Size = new Size(115, 25);
            lblIdFlight.TabIndex = 7;
            lblIdFlight.Text = "VN-6021";
            lblIdFlight.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // pictureBox13
            // 
            pictureBox13.Image = (Image)resources.GetObject("pictureBox13.Image");
            pictureBox13.Location = new Point(326, 84);
            pictureBox13.Name = "pictureBox13";
            pictureBox13.Size = new Size(128, 25);
            pictureBox13.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox13.TabIndex = 14;
            pictureBox13.TabStop = false;
            // 
            // imgHang
            // 
            imgHang.Image = (Image)resources.GetObject("imgHang.Image");
            imgHang.Location = new Point(11, 74);
            imgHang.Name = "imgHang";
            imgHang.Size = new Size(97, 51);
            imgHang.SizeMode = PictureBoxSizeMode.StretchImage;
            imgHang.TabIndex = 4;
            imgHang.TabStop = false;
            // 
            // listView1
            // 
            listView1.Location = new Point(3, 379);
            listView1.Name = "listView1";
            listView1.Size = new Size(971, 378);
            listView1.TabIndex = 2;
            listView1.UseCompatibleStateImageBehavior = false;
            // 
            // rdoQuickest
            // 
            rdoQuickest.AutoSize = true;
            rdoQuickest.Font = new Font("Segoe UI Semibold", 9F, FontStyle.Bold, GraphicsUnit.Point);
            rdoQuickest.Location = new Point(869, 20);
            rdoQuickest.Name = "rdoQuickest";
            rdoQuickest.Size = new Size(88, 24);
            rdoQuickest.TabIndex = 6;
            rdoQuickest.TabStop = true;
            rdoQuickest.Text = "Quickest";
            rdoQuickest.UseVisualStyleBackColor = true;
            // 
            // rdoBest
            // 
            rdoBest.AutoSize = true;
            rdoBest.Font = new Font("Segoe UI Semibold", 9F, FontStyle.Bold, GraphicsUnit.Point);
            rdoBest.Location = new Point(787, 20);
            rdoBest.Name = "rdoBest";
            rdoBest.Size = new Size(58, 24);
            rdoBest.TabIndex = 5;
            rdoBest.TabStop = true;
            rdoBest.Text = "Best";
            rdoBest.UseVisualStyleBackColor = true;
            // 
            // rdoCheapest
            // 
            rdoCheapest.AutoSize = true;
            rdoCheapest.Font = new Font("Segoe UI Semibold", 9F, FontStyle.Bold, GraphicsUnit.Point);
            rdoCheapest.Location = new Point(670, 20);
            rdoCheapest.Name = "rdoCheapest";
            rdoCheapest.Size = new Size(92, 24);
            rdoCheapest.TabIndex = 4;
            rdoCheapest.TabStop = true;
            rdoCheapest.Text = "Cheapest";
            rdoCheapest.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            label4.Font = new Font("SimSun", 10.8F, FontStyle.Bold, GraphicsUnit.Point);
            label4.ForeColor = Color.FromArgb(62, 146, 252);
            label4.Location = new Point(225, 22);
            label4.Name = "label4";
            label4.Size = new Size(108, 24);
            label4.TabIndex = 3;
            label4.Text = "300 result";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Segoe UI Emoji", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label3.Location = new Point(149, 16);
            label3.Name = "label3";
            label3.Size = new Size(49, 24);
            label3.TabIndex = 2;
            label3.Text = "Total";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            label2.Location = new Point(114, 14);
            label2.Name = "label2";
            label2.Size = new Size(17, 28);
            label2.TabIndex = 1;
            label2.Text = "|";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 13.8F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(3, 13);
            label1.Name = "label1";
            label1.Size = new Size(105, 31);
            label1.TabIndex = 0;
            label1.Text = "FLIGHTS";
            // 
            // panel5
            // 
            panel5.BackColor = SystemColors.ScrollBar;
            panel5.Controls.Add(rdoQuickest);
            panel5.Controls.Add(rdoBest);
            panel5.Controls.Add(rdoCheapest);
            panel5.Controls.Add(label4);
            panel5.Controls.Add(label3);
            panel5.Controls.Add(label2);
            panel5.Controls.Add(label1);
            panel5.ForeColor = SystemColors.ControlText;
            panel5.Location = new Point(3, 318);
            panel5.Name = "panel5";
            panel5.Size = new Size(971, 61);
            panel5.TabIndex = 1;
            // 
            // dtpReturnTime
            // 
            dtpReturnTime.Location = new Point(13, 74);
            dtpReturnTime.Name = "dtpReturnTime";
            dtpReturnTime.Size = new Size(250, 27);
            dtpReturnTime.TabIndex = 1;
            // 
            // cbbClass
            // 
            cbbClass.FormattingEnabled = true;
            cbbClass.Location = new Point(24, 27);
            cbbClass.Name = "cbbClass";
            cbbClass.Size = new Size(123, 28);
            cbbClass.TabIndex = 1;
            // 
            // groupBox4
            // 
            groupBox4.Controls.Add(cbbClass);
            groupBox4.Location = new Point(768, 35);
            groupBox4.Name = "groupBox4";
            groupBox4.Size = new Size(162, 70);
            groupBox4.TabIndex = 3;
            groupBox4.TabStop = false;
            groupBox4.Text = "CLASS";
            // 
            // txtTo
            // 
            txtTo.Location = new Point(16, 27);
            txtTo.Name = "txtTo";
            txtTo.Size = new Size(210, 27);
            txtTo.TabIndex = 0;
            // 
            // groupBox3
            // 
            groupBox3.Controls.Add(txtTo);
            groupBox3.Location = new Point(510, 35);
            groupBox3.Name = "groupBox3";
            groupBox3.Size = new Size(232, 70);
            groupBox3.TabIndex = 2;
            groupBox3.TabStop = false;
            groupBox3.Text = "TO";
            // 
            // txtFrom
            // 
            txtFrom.Location = new Point(16, 27);
            txtFrom.Name = "txtFrom";
            txtFrom.Size = new Size(210, 27);
            txtFrom.TabIndex = 0;
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(txtFrom);
            groupBox2.Location = new Point(242, 35);
            groupBox2.Name = "groupBox2";
            groupBox2.Size = new Size(232, 70);
            groupBox2.TabIndex = 1;
            groupBox2.TabStop = false;
            groupBox2.Text = "FROM";
            // 
            // cbbType
            // 
            cbbType.FormattingEnabled = true;
            cbbType.Items.AddRange(new object[] { "Onway", "RoundTrip" });
            cbbType.Location = new Point(6, 26);
            cbbType.Name = "cbbType";
            cbbType.Size = new Size(134, 28);
            cbbType.TabIndex = 0;
            cbbType.SelectedIndexChanged += cbbType_SelectedIndexChanged;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(cbbType);
            groupBox1.Location = new Point(40, 35);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(149, 70);
            groupBox1.TabIndex = 0;
            groupBox1.TabStop = false;
            groupBox1.Text = "TYPE";
            // 
            // panel2
            // 
            panel2.BackColor = Color.White;
            panel2.Controls.Add(radioButton3);
            panel2.Controls.Add(radioButton2);
            panel2.Controls.Add(radioButton1);
            panel2.Controls.Add(checkedListBox1);
            panel2.Controls.Add(comboBox2);
            panel2.Controls.Add(comboBox1);
            panel2.Controls.Add(foxLabel17);
            panel2.Controls.Add(foxLabel16);
            panel2.Controls.Add(foxLabel15);
            panel2.Controls.Add(foxLabel12);
            panel2.Controls.Add(panel7);
            panel2.Location = new Point(1030, 180);
            panel2.Name = "panel2";
            panel2.Size = new Size(413, 760);
            panel2.TabIndex = 8;
            // 
            // radioButton3
            // 
            radioButton3.AutoSize = true;
            radioButton3.Location = new Point(16, 235);
            radioButton3.Name = "radioButton3";
            radioButton3.Size = new Size(76, 24);
            radioButton3.TabIndex = 41;
            radioButton3.TabStop = true;
            radioButton3.Text = "> 900$";
            radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            radioButton2.AutoSize = true;
            radioButton2.Location = new Point(16, 174);
            radioButton2.Name = "radioButton2";
            radioButton2.Size = new Size(100, 24);
            radioButton2.TabIndex = 40;
            radioButton2.TabStop = true;
            radioButton2.Text = "300 - 900$";
            radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            radioButton1.AutoSize = true;
            radioButton1.Location = new Point(16, 114);
            radioButton1.Name = "radioButton1";
            radioButton1.Size = new Size(76, 24);
            radioButton1.TabIndex = 39;
            radioButton1.TabStop = true;
            radioButton1.Text = "< 300$";
            radioButton1.UseVisualStyleBackColor = true;
            // 
            // checkedListBox1
            // 
            checkedListBox1.BorderStyle = BorderStyle.None;
            checkedListBox1.FormattingEnabled = true;
            checkedListBox1.Items.AddRange(new object[] { "HCM", "HN", "DN", "CT", "PLK" });
            checkedListBox1.Location = new Point(16, 492);
            checkedListBox1.Name = "checkedListBox1";
            checkedListBox1.Size = new Size(150, 110);
            checkedListBox1.TabIndex = 38;
            // 
            // comboBox2
            // 
            comboBox2.FormattingEnabled = true;
            comboBox2.Location = new Point(16, 411);
            comboBox2.Name = "comboBox2";
            comboBox2.Size = new Size(258, 28);
            comboBox2.TabIndex = 37;
            // 
            // comboBox1
            // 
            comboBox1.FormattingEnabled = true;
            comboBox1.Location = new Point(16, 318);
            comboBox1.Name = "comboBox1";
            comboBox1.Size = new Size(258, 28);
            comboBox1.TabIndex = 36;
            // 
            // foxLabel17
            // 
            foxLabel17.Font = new Font("Segoe UI", 10F, FontStyle.Bold, GraphicsUnit.Point);
            foxLabel17.ForeColor = Color.FromArgb(64, 64, 64);
            foxLabel17.Location = new Point(16, 451);
            foxLabel17.Name = "foxLabel17";
            foxLabel17.Size = new Size(56, 24);
            foxLabel17.TabIndex = 34;
            foxLabel17.Text = "Airport";
            // 
            // foxLabel16
            // 
            foxLabel16.Font = new Font("Segoe UI", 10F, FontStyle.Bold, GraphicsUnit.Point);
            foxLabel16.ForeColor = Color.FromArgb(64, 64, 64);
            foxLabel16.Location = new Point(16, 371);
            foxLabel16.Name = "foxLabel16";
            foxLabel16.Size = new Size(56, 24);
            foxLabel16.TabIndex = 33;
            foxLabel16.Text = "Airlines";
            // 
            // foxLabel15
            // 
            foxLabel15.Font = new Font("Segoe UI", 10F, FontStyle.Bold, GraphicsUnit.Point);
            foxLabel15.ForeColor = Color.FromArgb(64, 64, 64);
            foxLabel15.Location = new Point(16, 275);
            foxLabel15.Name = "foxLabel15";
            foxLabel15.Size = new Size(56, 24);
            foxLabel15.TabIndex = 32;
            foxLabel15.Text = "Stop";
            // 
            // foxLabel12
            // 
            foxLabel12.Font = new Font("Segoe UI", 10F, FontStyle.Bold, GraphicsUnit.Point);
            foxLabel12.ForeColor = Color.FromArgb(64, 64, 64);
            foxLabel12.Location = new Point(16, 78);
            foxLabel12.Name = "foxLabel12";
            foxLabel12.Size = new Size(56, 24);
            foxLabel12.TabIndex = 31;
            foxLabel12.Text = "Price";
            // 
            // panel7
            // 
            panel7.Controls.Add(label6);
            panel7.Dock = DockStyle.Top;
            panel7.Location = new Point(0, 0);
            panel7.Name = "panel7";
            panel7.Size = new Size(413, 64);
            panel7.TabIndex = 0;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new Font("Segoe UI", 13.8F, FontStyle.Bold, GraphicsUnit.Point);
            label6.Location = new Point(16, 16);
            label6.Name = "label6";
            label6.Size = new Size(96, 31);
            label6.TabIndex = 1;
            label6.Text = "FILTERS";
            // 
            // groupBox5
            // 
            groupBox5.Controls.Add(dtpReturnTime);
            groupBox5.Controls.Add(dtpDepartTime);
            groupBox5.Location = new Point(970, 12);
            groupBox5.Name = "groupBox5";
            groupBox5.Size = new Size(269, 116);
            groupBox5.TabIndex = 4;
            groupBox5.TabStop = false;
            groupBox5.Text = "DEPARTURE - RETURN";
            // 
            // dtpDepartTime
            // 
            dtpDepartTime.Location = new Point(13, 28);
            dtpDepartTime.Name = "dtpDepartTime";
            dtpDepartTime.Size = new Size(250, 27);
            dtpDepartTime.TabIndex = 0;
            // 
            // panel1
            // 
            panel1.BackColor = Color.FromArgb(234, 239, 243);
            panel1.Controls.Add(btnBookNow);
            panel1.Controls.Add(listView1);
            panel1.Controls.Add(panel5);
            panel1.Controls.Add(panel3);
            panel1.Location = new Point(40, 180);
            panel1.Name = "panel1";
            panel1.Size = new Size(977, 760);
            panel1.TabIndex = 7;
            // 
            // btnBookNow
            // 
            btnBookNow.AutoSize = false;
            btnBookNow.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            btnBookNow.Cursor = Cursors.Hand;
            btnBookNow.Density = ReaLTaiizor.Controls.MaterialButton.MaterialButtonDensity.Default;
            btnBookNow.Depth = 0;
            btnBookNow.HighEmphasis = true;
            btnBookNow.Icon = null;
            btnBookNow.IconType = ReaLTaiizor.Controls.MaterialButton.MaterialIconType.Rebase;
            btnBookNow.Location = new Point(685, 261);
            btnBookNow.Margin = new Padding(4, 6, 4, 6);
            btnBookNow.MouseState = ReaLTaiizor.Helper.MaterialDrawHelper.MaterialMouseState.HOVER;
            btnBookNow.Name = "btnBookNow";
            btnBookNow.NoAccentTextColor = Color.Empty;
            btnBookNow.Size = new Size(289, 45);
            btnBookNow.TabIndex = 48;
            btnBookNow.Text = "BOOK NOW";
            btnBookNow.Type = ReaLTaiizor.Controls.MaterialButton.MaterialButtonType.Contained;
            btnBookNow.UseAccentColor = false;
            btnBookNow.UseVisualStyleBackColor = true;
            btnBookNow.Click += btnBookNow_Click_1;
            // 
            // panelOnway
            // 
            panelOnway.AccessibleRole = AccessibleRole.None;
            panelOnway.BackColor = Color.FromArgb(248, 249, 251);
            panelOnway.Controls.Add(btnSearch);
            panelOnway.Controls.Add(groupBox5);
            panelOnway.Controls.Add(groupBox4);
            panelOnway.Controls.Add(groupBox3);
            panelOnway.Controls.Add(groupBox2);
            panelOnway.Controls.Add(groupBox1);
            panelOnway.Dock = DockStyle.Top;
            panelOnway.Location = new Point(0, 0);
            panelOnway.Name = "panelOnway";
            panelOnway.Size = new Size(1503, 146);
            panelOnway.TabIndex = 10;
            // 
            // btnSearch
            // 
            btnSearch.BorderColor = Color.FromArgb(220, 223, 230);
            btnSearch.ButtonType = ReaLTaiizor.Util.HopeButtonType.Primary;
            btnSearch.DangerColor = Color.FromArgb(245, 108, 108);
            btnSearch.DefaultColor = Color.FromArgb(255, 255, 255);
            btnSearch.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnSearch.HoverTextColor = Color.FromArgb(48, 49, 51);
            btnSearch.InfoColor = Color.FromArgb(144, 147, 153);
            btnSearch.Location = new Point(1259, 45);
            btnSearch.Name = "btnSearch";
            btnSearch.PrimaryColor = Color.FromArgb(224, 20, 45);
            btnSearch.Size = new Size(184, 50);
            btnSearch.SuccessColor = Color.FromArgb(103, 194, 58);
            btnSearch.TabIndex = 0;
            btnSearch.Text = "Search Flight";
            btnSearch.TextColor = Color.White;
            btnSearch.WarningColor = Color.FromArgb(230, 162, 60);
            // 
            // panel4
            // 
            panel4.BackColor = Color.White;
            panel4.Controls.Add(label14);
            panel4.Controls.Add(label13);
            panel4.Controls.Add(lblPriceDiscount);
            panel4.Controls.Add(ForPeople);
            panel4.Controls.Add(label12);
            panel4.Controls.Add(pictureBox2);
            panel4.Controls.Add(lblPeiceChildren);
            panel4.Controls.Add(lblPriceAdult);
            panel4.Controls.Add(label11);
            panel4.Controls.Add(label10);
            panel4.Controls.Add(label9);
            panel4.Controls.Add(lblOld);
            panel4.Controls.Add(label8);
            panel4.Location = new Point(725, 183);
            panel4.Name = "panel4";
            panel4.Size = new Size(289, 256);
            panel4.TabIndex = 3;
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.ForeColor = SystemColors.ControlDarkDark;
            label14.Location = new Point(192, 234);
            label14.Name = "label14";
            label14.Size = new Size(91, 20);
            label14.TabIndex = 68;
            label14.Text = "Tax included";
            // 
            // label13
            // 
            label13.BackColor = Color.White;
            label13.Font = new Font("Segoe UI", 13.8F, FontStyle.Bold, GraphicsUnit.Point);
            label13.ForeColor = Color.Red;
            label13.Location = new Point(105, 197);
            label13.Name = "label13";
            label13.RightToLeft = RightToLeft.No;
            label13.Size = new Size(178, 40);
            label13.TabIndex = 67;
            label13.Text = "$590";
            label13.TextAlign = ContentAlignment.MiddleRight;
            // 
            // lblPriceDiscount
            // 
            lblPriceDiscount.BackColor = Color.White;
            lblPriceDiscount.ForeColor = Color.LimeGreen;
            lblPriceDiscount.Location = new Point(217, 127);
            lblPriceDiscount.Name = "lblPriceDiscount";
            lblPriceDiscount.Size = new Size(69, 25);
            lblPriceDiscount.TabIndex = 66;
            lblPriceDiscount.Text = "$9";
            lblPriceDiscount.TextAlign = ContentAlignment.TopRight;
            // 
            // ForPeople
            // 
            ForPeople.AutoSize = true;
            ForPeople.Location = new Point(3, 234);
            ForPeople.Name = "ForPeople";
            ForPeople.Size = new Size(93, 20);
            ForPeople.TabIndex = 65;
            ForPeople.Text = "For 2 people";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label12.Location = new Point(3, 207);
            label12.Name = "label12";
            label12.Size = new Size(44, 20);
            label12.TabIndex = 64;
            label12.Text = "Total";
            // 
            // pictureBox2
            // 
            pictureBox2.Image = (Image)resources.GetObject("pictureBox2.Image");
            pictureBox2.Location = new Point(3, 179);
            pictureBox2.Name = "pictureBox2";
            pictureBox2.Size = new Size(283, 10);
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox2.TabIndex = 63;
            pictureBox2.TabStop = false;
            // 
            // lblPeiceChildren
            // 
            lblPeiceChildren.Location = new Point(217, 102);
            lblPeiceChildren.Name = "lblPeiceChildren";
            lblPeiceChildren.Size = new Size(69, 25);
            lblPeiceChildren.TabIndex = 7;
            lblPeiceChildren.Text = "$299";
            lblPeiceChildren.TextAlign = ContentAlignment.TopRight;
            // 
            // lblPriceAdult
            // 
            lblPriceAdult.Location = new Point(217, 74);
            lblPriceAdult.Name = "lblPriceAdult";
            lblPriceAdult.Size = new Size(69, 25);
            lblPriceAdult.TabIndex = 6;
            lblPriceAdult.Text = "$300";
            lblPriceAdult.TextAlign = ContentAlignment.TopRight;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new Point(3, 135);
            label11.Name = "label11";
            label11.Size = new Size(67, 20);
            label11.TabIndex = 5;
            label11.Text = "Discount";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Location = new Point(3, 107);
            label10.Name = "label10";
            label10.Size = new Size(93, 20);
            label10.TabIndex = 4;
            label10.Text = "Children (x1)";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label9.Location = new Point(3, 51);
            label9.Name = "label9";
            label9.Size = new Size(167, 20);
            label9.TabIndex = 3;
            label9.Text = "Departure (SGN-HAN)";
            // 
            // lblOld
            // 
            lblOld.AutoSize = true;
            lblOld.Location = new Point(3, 79);
            lblOld.Name = "lblOld";
            lblOld.Size = new Size(74, 20);
            lblOld.TabIndex = 2;
            lblOld.Text = "Adult (x1)";
            // 
            // label8
            // 
            label8.BorderStyle = BorderStyle.FixedSingle;
            label8.Font = new Font("Segoe UI", 10.8F, FontStyle.Bold, GraphicsUnit.Point);
            label8.Location = new Point(3, 5);
            label8.Name = "label8";
            label8.Size = new Size(283, 33);
            label8.TabIndex = 1;
            label8.Text = "Price details";
            label8.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // button1
            // 
            button1.Location = new Point(526, 260);
            button1.Name = "button1";
            button1.Size = new Size(94, 29);
            button1.TabIndex = 66;
            button1.Text = "button1";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // Form_Flight
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.FromArgb(234, 239, 243);
            ClientSize = new Size(1503, 949);
            Controls.Add(panel4);
            Controls.Add(panel2);
            Controls.Add(panel1);
            Controls.Add(panelOnway);
            Name = "Form_Flight";
            Text = "Form3";
            Load += Form_Flight_Load;
            panel3.ResumeLayout(false);
            panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox3).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox11).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox10).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox6).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox5).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox13).EndInit();
            ((System.ComponentModel.ISupportInitialize)imgHang).EndInit();
            panel5.ResumeLayout(false);
            panel5.PerformLayout();
            groupBox4.ResumeLayout(false);
            groupBox3.ResumeLayout(false);
            groupBox3.PerformLayout();
            groupBox2.ResumeLayout(false);
            groupBox2.PerformLayout();
            groupBox1.ResumeLayout(false);
            panel2.ResumeLayout(false);
            panel2.PerformLayout();
            panel7.ResumeLayout(false);
            panel7.PerformLayout();
            groupBox5.ResumeLayout(false);
            panel1.ResumeLayout(false);
            panelOnway.ResumeLayout(false);
            panel4.ResumeLayout(false);
            panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox2).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private Panel panel3;
        private Label lblNameAirline;
        private PictureBox pictureBox3;
        private Label label17;
        private Label lblDepartAirport;
        private Label label16;
        private ReaLTaiizor.Controls.FoxLabel foxLabel21;
        private ReaLTaiizor.Controls.FoxLabel lblCabin;
        private ReaLTaiizor.Controls.FoxLabel foxLabel14;
        private Label label23;
        private ReaLTaiizor.Controls.FoxLabel lblCheckIn;
        private ReaLTaiizor.Controls.FoxLabel foxLabel13;
        private ReaLTaiizor.Controls.FoxLabel lblBaggage;
        private PictureBox pictureBox11;
        private Label label22;
        private Label lblTotalFlightTime;
        private PictureBox pictureBox10;
        private Label label21;
        private Label label5;
        private PictureBox pictureBox6;
        private Label lblFinishTime;
        private Label label20;
        private Label lblStartTime;
        private PictureBox pictureBox5;
        private Label lblRate;
        private ReaLTaiizor.Controls.NightLabel nightLabel2;
        private Label lblIdFlight;
        private PictureBox pictureBox13;
        private PictureBox imgHang;
        private ListView listView1;
        private RadioButton rdoQuickest;
        private RadioButton rdoBest;
        private RadioButton rdoCheapest;
        private Label label4;
        private Label label3;
        private Label label2;
        private Label label1;
        private Panel panel5;
        private DateTimePicker dtpReturnTime;
        private ComboBox cbbClass;
        public GroupBox groupBox4;
        private TextBox txtTo;
        public GroupBox groupBox3;
        private TextBox txtFrom;
        public GroupBox groupBox2;
        private ComboBox cbbType;
        private GroupBox groupBox1;
        private Panel panel2;
        private RadioButton radioButton3;
        private RadioButton radioButton2;
        private RadioButton radioButton1;
        private CheckedListBox checkedListBox1;
        private ComboBox comboBox2;
        private ComboBox comboBox1;
        private ReaLTaiizor.Controls.FoxLabel foxLabel17;
        private ReaLTaiizor.Controls.FoxLabel foxLabel16;
        private ReaLTaiizor.Controls.FoxLabel foxLabel15;
        private ReaLTaiizor.Controls.FoxLabel foxLabel12;
        private Panel panel7;
        private Label label6;
        public GroupBox groupBox5;
        private DateTimePicker dtpDepartTime;
        private Panel panel1;
        private Panel panelOnway;
        private ReaLTaiizor.Controls.HopeRoundButton btnSearch;
        private Label label7;
        private PictureBox pictureBox1;
        private Panel panel4;
        private Label lblOld;
        private Label label8;
        private Label label9;
        private Label lblPeiceChildren;
        private Label lblPriceAdult;
        private Label label11;
        private Label label10;
        private Label label13;
        private Label lblPriceDiscount;
        private Label ForPeople;
        private Label label12;
        private PictureBox pictureBox2;
        private Label label14;
        private ReaLTaiizor.Controls.MaterialButton btnBookNow;
        private Button button1;
    }
}