﻿namespace Projectc_
{
    partial class FormAddPersonel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAddPersonel));
            txtSalary = new TextBox();
            txtPosition = new TextBox();
            txtShift = new TextBox();
            label12 = new Label();
            label8 = new Label();
            label6 = new Label();
            txtPhoneNumber = new TextBox();
            label11 = new Label();
            txtIdCitizen = new TextBox();
            label10 = new Label();
            dateTimePickerDateOfBirth = new DateTimePicker();
            label9 = new Label();
            txtGender = new TextBox();
            label7 = new Label();
            txtPersonelName = new TextBox();
            Name = new Label();
            txtPersonelFamily = new TextBox();
            label5 = new Label();
            label4 = new Label();
            btnAdd = new Button();
            btnCancel = new Button();
            SuspendLayout();
            // 
            // txtSalary
            // 
            txtSalary.Location = new Point(174, 522);
            txtSalary.Name = "txtSalary";
            txtSalary.Size = new Size(268, 27);
            txtSalary.TabIndex = 50;
            // 
            // txtPosition
            // 
            txtPosition.Location = new Point(174, 470);
            txtPosition.Name = "txtPosition";
            txtPosition.Size = new Size(268, 27);
            txtPosition.TabIndex = 49;
            // 
            // txtShift
            // 
            txtShift.Location = new Point(174, 418);
            txtShift.Name = "txtShift";
            txtShift.Size = new Size(268, 27);
            txtShift.TabIndex = 48;
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label12.ForeColor = Color.White;
            label12.Location = new Point(13, 523);
            label12.Name = "label12";
            label12.Size = new Size(60, 23);
            label12.TabIndex = 47;
            label12.Text = "Salary";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label8.ForeColor = Color.White;
            label8.Location = new Point(13, 470);
            label8.Name = "label8";
            label8.Size = new Size(73, 23);
            label8.TabIndex = 46;
            label8.Text = "Position";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label6.ForeColor = Color.White;
            label6.Location = new Point(13, 419);
            label6.Name = "label6";
            label6.Size = new Size(49, 23);
            label6.TabIndex = 45;
            label6.Text = "Shift";
            // 
            // txtPhoneNumber
            // 
            txtPhoneNumber.Location = new Point(176, 367);
            txtPhoneNumber.Name = "txtPhoneNumber";
            txtPhoneNumber.Size = new Size(266, 27);
            txtPhoneNumber.TabIndex = 42;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label11.ForeColor = Color.White;
            label11.Location = new Point(13, 367);
            label11.Name = "label11";
            label11.Size = new Size(130, 23);
            label11.TabIndex = 41;
            label11.Text = "Phone Number";
            // 
            // txtIdCitizen
            // 
            txtIdCitizen.Location = new Point(176, 315);
            txtIdCitizen.Name = "txtIdCitizen";
            txtIdCitizen.Size = new Size(266, 27);
            txtIdCitizen.TabIndex = 40;
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label10.ForeColor = Color.White;
            label10.Location = new Point(13, 319);
            label10.Name = "label10";
            label10.Size = new Size(86, 23);
            label10.TabIndex = 39;
            label10.Text = "Id Citizen";
            // 
            // dateTimePickerDateOfBirth
            // 
            dateTimePickerDateOfBirth.Location = new Point(176, 266);
            dateTimePickerDateOfBirth.Name = "dateTimePickerDateOfBirth";
            dateTimePickerDateOfBirth.Size = new Size(266, 27);
            dateTimePickerDateOfBirth.TabIndex = 38;
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label9.ForeColor = Color.White;
            label9.Location = new Point(13, 270);
            label9.Name = "label9";
            label9.Size = new Size(115, 23);
            label9.TabIndex = 37;
            label9.Text = "Date of birth";
            // 
            // txtGender
            // 
            txtGender.Location = new Point(176, 208);
            txtGender.Name = "txtGender";
            txtGender.Size = new Size(266, 27);
            txtGender.TabIndex = 36;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label7.ForeColor = Color.White;
            label7.Location = new Point(13, 212);
            label7.Name = "label7";
            label7.Size = new Size(68, 23);
            label7.TabIndex = 35;
            label7.Text = "Gender";
            // 
            // txtPersonelName
            // 
            txtPersonelName.Location = new Point(176, 157);
            txtPersonelName.Name = "txtPersonelName";
            txtPersonelName.Size = new Size(266, 27);
            txtPersonelName.TabIndex = 34;
            // 
            // Name
            // 
            Name.AutoSize = true;
            Name.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            Name.ForeColor = Color.White;
            Name.Location = new Point(13, 161);
            Name.Name = "Name";
            Name.Size = new Size(128, 23);
            Name.TabIndex = 33;
            Name.Text = "Personel Name";
            // 
            // txtPersonelFamily
            // 
            txtPersonelFamily.Location = new Point(176, 101);
            txtPersonelFamily.Name = "txtPersonelFamily";
            txtPersonelFamily.Size = new Size(266, 27);
            txtPersonelFamily.TabIndex = 32;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label5.ForeColor = Color.White;
            label5.Location = new Point(13, 102);
            label5.Name = "label5";
            label5.Size = new Size(132, 23);
            label5.TabIndex = 31;
            label5.Text = "Personel family";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Segoe UI", 15F, FontStyle.Bold, GraphicsUnit.Point);
            label4.ForeColor = Color.White;
            label4.Location = new Point(41, 25);
            label4.Name = "label4";
            label4.Size = new Size(365, 35);
            label4.TabIndex = 30;
            label4.Text = "detailed Personel Information";
            // 
            // btnAdd
            // 
            btnAdd.Font = new Font("Segoe UI", 10.8F, FontStyle.Bold, GraphicsUnit.Point);
            btnAdd.Image = (Image)resources.GetObject("btnAdd.Image");
            btnAdd.ImageAlign = ContentAlignment.MiddleLeft;
            btnAdd.Location = new Point(32, 573);
            btnAdd.Name = "btnAdd";
            btnAdd.Size = new Size(140, 47);
            btnAdd.TabIndex = 51;
            btnAdd.Text = "    Add";
            btnAdd.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            btnCancel.Font = new Font("Segoe UI", 10.8F, FontStyle.Bold, GraphicsUnit.Point);
            btnCancel.Image = (Image)resources.GetObject("btnCancel.Image");
            btnCancel.ImageAlign = ContentAlignment.MiddleLeft;
            btnCancel.Location = new Point(257, 573);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new Size(140, 47);
            btnCancel.TabIndex = 52;
            btnCancel.Text = "    Cancel";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Click += btnCancel_Click;
            // 
            // FormAddPersonel
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.Black;
            ClientSize = new Size(475, 649);
            Controls.Add(btnCancel);
            Controls.Add(btnAdd);
            Controls.Add(txtSalary);
            Controls.Add(txtPosition);
            Controls.Add(txtShift);
            Controls.Add(label12);
            Controls.Add(label8);
            Controls.Add(label6);
            Controls.Add(txtPhoneNumber);
            Controls.Add(label11);
            Controls.Add(txtIdCitizen);
            Controls.Add(label10);
            Controls.Add(dateTimePickerDateOfBirth);
            Controls.Add(label9);
            Controls.Add(txtGender);
            Controls.Add(label7);
            Controls.Add(txtPersonelName);
            Controls.Add(Name);
            Controls.Add(txtPersonelFamily);
            Controls.Add(label5);
            Controls.Add(label4);
            FormBorderStyle = FormBorderStyle.None;
            StartPosition = FormStartPosition.CenterScreen;
            Text = "AddPersonel";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private TextBox txtSalary;
        private TextBox txtPosition;
        private TextBox txtShift;
        private Label label12;
        private Label label8;
        private Label label6;
        private TextBox txtPhoneNumber;
        private Label label11;
        private TextBox txtIdCitizen;
        private Label label10;
        private DateTimePicker dateTimePickerDateOfBirth;
        private Label label9;
        private TextBox txtGender;
        private Label label7;
        private TextBox txtPersonelName;
        private Label Name;
        private TextBox txtPersonelFamily;
        private Label label5;
        private Label label4;
        private Button btnAdd;
        private Button btnCancel;
    }
}