﻿namespace Projectc_
{
    partial class FormPersonel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPersonel));
            btnLoad = new Button();
            panel2 = new Panel();
            btnSearch = new Button();
            txtSearch = new TextBox();
            label2 = new Label();
            label3 = new Label();
            btnAddPersonel = new Button();
            panel1 = new Panel();
            txtSalary = new TextBox();
            txtPosition = new TextBox();
            txtShift = new TextBox();
            label12 = new Label();
            label8 = new Label();
            label6 = new Label();
            btnDelete = new Button();
            btnEdit = new Button();
            txtPhoneNumber = new TextBox();
            label11 = new Label();
            txtIdCitizen = new TextBox();
            label10 = new Label();
            dateTimePickerDateOfBirth = new DateTimePicker();
            label9 = new Label();
            txtGender = new TextBox();
            label7 = new Label();
            txtPersonelName = new TextBox();
            Name = new Label();
            txtPersonelFamily = new TextBox();
            label5 = new Label();
            label4 = new Label();
            dataGridViewClients = new DataGridView();
            label1 = new Label();
            panel2.SuspendLayout();
            panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridViewClients).BeginInit();
            SuspendLayout();
            // 
            // btnLoad
            // 
            btnLoad.Anchor = AnchorStyles.Top;
            btnLoad.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btnLoad.Image = (Image)resources.GetObject("btnLoad.Image");
            btnLoad.ImageAlign = ContentAlignment.MiddleLeft;
            btnLoad.Location = new Point(722, 125);
            btnLoad.Name = "btnLoad";
            btnLoad.Size = new Size(164, 36);
            btnLoad.TabIndex = 13;
            btnLoad.Text = "Load";
            btnLoad.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            panel2.Anchor = AnchorStyles.Top;
            panel2.Controls.Add(btnSearch);
            panel2.Controls.Add(txtSearch);
            panel2.Controls.Add(label2);
            panel2.Controls.Add(label3);
            panel2.Controls.Add(btnAddPersonel);
            panel2.Location = new Point(15, 61);
            panel2.Name = "panel2";
            panel2.Size = new Size(1428, 58);
            panel2.TabIndex = 12;
            // 
            // btnSearch
            // 
            btnSearch.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btnSearch.Image = (Image)resources.GetObject("btnSearch.Image");
            btnSearch.ImageAlign = ContentAlignment.MiddleLeft;
            btnSearch.Location = new Point(403, 6);
            btnSearch.Name = "btnSearch";
            btnSearch.Size = new Size(143, 35);
            btnSearch.TabIndex = 3;
            btnSearch.Text = "Search";
            btnSearch.UseVisualStyleBackColor = true;
            // 
            // txtSearch
            // 
            txtSearch.Font = new Font("Segoe UI Light", 9F, FontStyle.Italic, GraphicsUnit.Point);
            txtSearch.Location = new Point(84, 11);
            txtSearch.Name = "txtSearch";
            txtSearch.Size = new Size(297, 27);
            txtSearch.TabIndex = 2;
            txtSearch.Text = "Name";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            label2.Location = new Point(3, 10);
            label2.Name = "label2";
            label2.Size = new Size(75, 28);
            label2.TabIndex = 1;
            label2.Text = "Search";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            label3.Location = new Point(1182, 13);
            label3.Name = "label3";
            label3.Size = new Size(94, 28);
            label3.TabIndex = 4;
            label3.Text = "Function";
            // 
            // btnAddPersonel
            // 
            btnAddPersonel.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btnAddPersonel.Image = (Image)resources.GetObject("btnAddPersonel.Image");
            btnAddPersonel.ImageAlign = ContentAlignment.MiddleLeft;
            btnAddPersonel.Location = new Point(1282, 11);
            btnAddPersonel.Name = "btnAddPersonel";
            btnAddPersonel.Size = new Size(143, 35);
            btnAddPersonel.TabIndex = 5;
            btnAddPersonel.Text = "   Add Personel";
            btnAddPersonel.UseVisualStyleBackColor = true;
            btnAddPersonel.Click += btnAddPersonel_Click;
            // 
            // panel1
            // 
            panel1.Anchor = AnchorStyles.Top;
            panel1.Controls.Add(txtSalary);
            panel1.Controls.Add(txtPosition);
            panel1.Controls.Add(txtShift);
            panel1.Controls.Add(label12);
            panel1.Controls.Add(label8);
            panel1.Controls.Add(label6);
            panel1.Controls.Add(btnDelete);
            panel1.Controls.Add(btnEdit);
            panel1.Controls.Add(txtPhoneNumber);
            panel1.Controls.Add(label11);
            panel1.Controls.Add(txtIdCitizen);
            panel1.Controls.Add(label10);
            panel1.Controls.Add(dateTimePickerDateOfBirth);
            panel1.Controls.Add(label9);
            panel1.Controls.Add(txtGender);
            panel1.Controls.Add(label7);
            panel1.Controls.Add(txtPersonelName);
            panel1.Controls.Add(Name);
            panel1.Controls.Add(txtPersonelFamily);
            panel1.Controls.Add(label5);
            panel1.Controls.Add(label4);
            panel1.Location = new Point(917, 167);
            panel1.Name = "panel1";
            panel1.Size = new Size(524, 699);
            panel1.TabIndex = 11;
            // 
            // txtSalary
            // 
            txtSalary.Location = new Point(188, 565);
            txtSalary.Name = "txtSalary";
            txtSalary.Size = new Size(310, 27);
            txtSalary.TabIndex = 29;
            // 
            // txtPosition
            // 
            txtPosition.Location = new Point(189, 505);
            txtPosition.Name = "txtPosition";
            txtPosition.Size = new Size(310, 27);
            txtPosition.TabIndex = 28;
            // 
            // txtShift
            // 
            txtShift.Location = new Point(188, 443);
            txtShift.Name = "txtShift";
            txtShift.Size = new Size(310, 27);
            txtShift.TabIndex = 27;
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label12.Location = new Point(12, 565);
            label12.Name = "label12";
            label12.Size = new Size(60, 23);
            label12.TabIndex = 26;
            label12.Text = "Salary";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label8.Location = new Point(12, 505);
            label8.Name = "label8";
            label8.Size = new Size(73, 23);
            label8.TabIndex = 25;
            label8.Text = "Position";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label6.Location = new Point(12, 443);
            label6.Name = "label6";
            label6.Size = new Size(49, 23);
            label6.TabIndex = 24;
            label6.Text = "Shift";
            // 
            // btnDelete
            // 
            btnDelete.Font = new Font("Segoe UI", 10.8F, FontStyle.Bold, GraphicsUnit.Point);
            btnDelete.Image = (Image)resources.GetObject("btnDelete.Image");
            btnDelete.ImageAlign = ContentAlignment.MiddleLeft;
            btnDelete.Location = new Point(280, 637);
            btnDelete.Name = "btnDelete";
            btnDelete.Size = new Size(140, 47);
            btnDelete.TabIndex = 23;
            btnDelete.Text = "    Delete";
            btnDelete.UseVisualStyleBackColor = true;
            // 
            // btnEdit
            // 
            btnEdit.Font = new Font("Segoe UI", 10.8F, FontStyle.Bold, GraphicsUnit.Point);
            btnEdit.Image = (Image)resources.GetObject("btnEdit.Image");
            btnEdit.ImageAlign = ContentAlignment.MiddleLeft;
            btnEdit.Location = new Point(99, 637);
            btnEdit.Name = "btnEdit";
            btnEdit.Size = new Size(140, 47);
            btnEdit.TabIndex = 22;
            btnEdit.Text = "    Edit";
            btnEdit.UseVisualStyleBackColor = true;
            // 
            // txtPhoneNumber
            // 
            txtPhoneNumber.Location = new Point(188, 382);
            txtPhoneNumber.Name = "txtPhoneNumber";
            txtPhoneNumber.Size = new Size(310, 27);
            txtPhoneNumber.TabIndex = 21;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label11.Location = new Point(12, 382);
            label11.Name = "label11";
            label11.Size = new Size(130, 23);
            label11.TabIndex = 20;
            label11.Text = "Phone Number";
            // 
            // txtIdCitizen
            // 
            txtIdCitizen.Location = new Point(188, 318);
            txtIdCitizen.Name = "txtIdCitizen";
            txtIdCitizen.Size = new Size(310, 27);
            txtIdCitizen.TabIndex = 19;
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label10.Location = new Point(12, 322);
            label10.Name = "label10";
            label10.Size = new Size(86, 23);
            label10.TabIndex = 18;
            label10.Text = "Id Citizen";
            // 
            // dateTimePickerDateOfBirth
            // 
            dateTimePickerDateOfBirth.Location = new Point(188, 255);
            dateTimePickerDateOfBirth.Name = "dateTimePickerDateOfBirth";
            dateTimePickerDateOfBirth.Size = new Size(310, 27);
            dateTimePickerDateOfBirth.TabIndex = 17;
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label9.Location = new Point(12, 258);
            label9.Name = "label9";
            label9.Size = new Size(115, 23);
            label9.TabIndex = 16;
            label9.Text = "Date of birth";
            // 
            // txtGender
            // 
            txtGender.Location = new Point(189, 192);
            txtGender.Name = "txtGender";
            txtGender.Size = new Size(309, 27);
            txtGender.TabIndex = 13;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label7.Location = new Point(12, 193);
            label7.Name = "label7";
            label7.Size = new Size(68, 23);
            label7.TabIndex = 12;
            label7.Text = "Gender";
            // 
            // txtPersonelName
            // 
            txtPersonelName.Location = new Point(189, 135);
            txtPersonelName.Name = "txtPersonelName";
            txtPersonelName.Size = new Size(309, 27);
            txtPersonelName.TabIndex = 11;
            // 
            // Name
            // 
            Name.AutoSize = true;
            Name.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            Name.Location = new Point(12, 136);
            Name.Name = "Name";
            Name.Size = new Size(128, 23);
            Name.TabIndex = 10;
            Name.Text = "Personel Name";
            // 
            // txtPersonelFamily
            // 
            txtPersonelFamily.Location = new Point(189, 74);
            txtPersonelFamily.Name = "txtPersonelFamily";
            txtPersonelFamily.Size = new Size(309, 27);
            txtPersonelFamily.TabIndex = 9;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label5.Location = new Point(12, 78);
            label5.Name = "label5";
            label5.Size = new Size(132, 23);
            label5.TabIndex = 8;
            label5.Text = "Personel family";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Segoe UI", 15F, FontStyle.Bold, GraphicsUnit.Point);
            label4.Location = new Point(99, 13);
            label4.Name = "label4";
            label4.Size = new Size(365, 35);
            label4.TabIndex = 8;
            label4.Text = "detailed Personel Information";
            // 
            // dataGridViewClients
            // 
            dataGridViewClients.Anchor = AnchorStyles.Top;
            dataGridViewClients.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewClients.Location = new Point(13, 167);
            dataGridViewClients.Name = "dataGridViewClients";
            dataGridViewClients.RowHeadersWidth = 51;
            dataGridViewClients.RowTemplate.Height = 29;
            dataGridViewClients.Size = new Size(873, 747);
            dataGridViewClients.TabIndex = 10;
            // 
            // label1
            // 
            label1.Anchor = AnchorStyles.Top;
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 22.2F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(-7, -5);
            label1.Name = "label1";
            label1.Size = new Size(171, 50);
            label1.TabIndex = 14;
            label1.Text = "Personel";
            // 
            // FormPersonel
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1455, 949);
            Controls.Add(label1);
            Controls.Add(btnLoad);
            Controls.Add(panel2);
            Controls.Add(panel1);
            Controls.Add(dataGridViewClients);
            Text = "FormPersonel";
            panel2.ResumeLayout(false);
            panel2.PerformLayout();
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridViewClients).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnLoad;
        private Panel panel2;
        private Button btnSearch;
        private TextBox txtSearch;
        private Label label2;
        private Label label3;
        private Button btnAddPersonel;
        private Panel panel1;
        private Button btnDelete;
        private Button btnEdit;
        private TextBox txtPhoneNumber;
        private Label label11;
        private TextBox txtIdCitizen;
        private Label label10;
        private DateTimePicker dateTimePickerDateOfBirth;
        private Label label9;
        private TextBox txtGender;
        private Label label7;
        private TextBox txtPersonelName;
        private Label Name;
        private TextBox txtPersonelFamily;
        private Label label5;
        private Label label4;
        private DataGridView dataGridViewClients;
        private Label label1;
        private TextBox txtSalary;
        private TextBox txtPosition;
        private TextBox txtShift;
        private Label label12;
        private Label label8;
        private Label label6;
    }
}