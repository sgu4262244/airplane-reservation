﻿namespace Projectc_
{
    partial class FormBill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBill));
            panelPrint = new Panel();
            panel6 = new Panel();
            ValueTotalAmount = new Label();
            label13 = new Label();
            panel5 = new Panel();
            dataGridView1 = new DataGridView();
            label4 = new Label();
            panel3 = new Panel();
            ValueDate = new Label();
            ValueCustomerName = new Label();
            ValueEmployeeName = new Label();
            ValueCodeBill = new Label();
            label5 = new Label();
            Name = new Label();
            label11 = new Label();
            label7 = new Label();
            label9 = new Label();
            panel4 = new Panel();
            panel2 = new Panel();
            panel1 = new Panel();
            label1 = new Label();
            pictureBox1 = new PictureBox();
            printPreviewDialog1 = new PrintPreviewDialog();
            pictureBox2 = new PictureBox();
            printDocument1 = new System.Drawing.Printing.PrintDocument();
            toolTip1 = new ToolTip(components);
            panelPrint.SuspendLayout();
            panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView1).BeginInit();
            panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox2).BeginInit();
            SuspendLayout();
            // 
            // panelPrint
            // 
            panelPrint.Controls.Add(panel6);
            panelPrint.Controls.Add(ValueTotalAmount);
            panelPrint.Controls.Add(label13);
            panelPrint.Controls.Add(panel5);
            panelPrint.Controls.Add(panel3);
            panelPrint.Controls.Add(panel4);
            panelPrint.Controls.Add(panel2);
            panelPrint.Controls.Add(panel1);
            panelPrint.Controls.Add(label1);
            panelPrint.Controls.Add(pictureBox1);
            panelPrint.Location = new Point(12, 35);
            panelPrint.Name = "panelPrint";
            panelPrint.Size = new Size(625, 631);
            panelPrint.TabIndex = 0;
            // 
            // panel6
            // 
            panel6.BackColor = Color.Silver;
            panel6.Location = new Point(13, 12);
            panel6.Name = "panel6";
            panel6.Size = new Size(600, 2);
            panel6.TabIndex = 59;
            // 
            // ValueTotalAmount
            // 
            ValueTotalAmount.AutoSize = true;
            ValueTotalAmount.Location = new Point(127, 577);
            ValueTotalAmount.Name = "ValueTotalAmount";
            ValueTotalAmount.Size = new Size(12, 20);
            ValueTotalAmount.TabIndex = 64;
            ValueTotalAmount.Text = ".";
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label13.Location = new Point(13, 577);
            label13.Name = "label13";
            label13.Size = new Size(106, 20);
            label13.TabIndex = 65;
            label13.Text = "Total Amount";
            // 
            // panel5
            // 
            panel5.Controls.Add(dataGridView1);
            panel5.Controls.Add(label4);
            panel5.Location = new Point(13, 298);
            panel5.Name = "panel5";
            panel5.Size = new Size(600, 226);
            panel5.TabIndex = 63;
            // 
            // dataGridView1
            // 
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridView1.Location = new Point(13, 37);
            dataGridView1.Name = "dataGridView1";
            dataGridView1.RowHeadersWidth = 51;
            dataGridView1.Size = new Size(569, 168);
            dataGridView1.TabIndex = 50;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label4.Location = new Point(3, 195);
            label4.Name = "label4";
            label4.Size = new Size(0, 23);
            label4.TabIndex = 36;
            // 
            // panel3
            // 
            panel3.Controls.Add(ValueDate);
            panel3.Controls.Add(ValueCustomerName);
            panel3.Controls.Add(ValueEmployeeName);
            panel3.Controls.Add(ValueCodeBill);
            panel3.Controls.Add(label5);
            panel3.Controls.Add(Name);
            panel3.Controls.Add(label11);
            panel3.Controls.Add(label7);
            panel3.Controls.Add(label9);
            panel3.Location = new Point(13, 114);
            panel3.Name = "panel3";
            panel3.Size = new Size(600, 152);
            panel3.TabIndex = 62;
            // 
            // ValueDate
            // 
            ValueDate.AutoSize = true;
            ValueDate.Location = new Point(170, 117);
            ValueDate.Name = "ValueDate";
            ValueDate.Size = new Size(12, 20);
            ValueDate.TabIndex = 54;
            ValueDate.Text = ".";
            // 
            // ValueCustomerName
            // 
            ValueCustomerName.AutoSize = true;
            ValueCustomerName.Location = new Point(170, 77);
            ValueCustomerName.Name = "ValueCustomerName";
            ValueCustomerName.Size = new Size(12, 20);
            ValueCustomerName.TabIndex = 54;
            ValueCustomerName.Text = ".";
            // 
            // ValueEmployeeName
            // 
            ValueEmployeeName.AutoSize = true;
            ValueEmployeeName.Location = new Point(170, 45);
            ValueEmployeeName.Name = "ValueEmployeeName";
            ValueEmployeeName.Size = new Size(12, 20);
            ValueEmployeeName.TabIndex = 54;
            ValueEmployeeName.Text = ".";
            // 
            // ValueCodeBill
            // 
            ValueCodeBill.AutoSize = true;
            ValueCodeBill.Location = new Point(170, 9);
            ValueCodeBill.Name = "ValueCodeBill";
            ValueCodeBill.Size = new Size(12, 20);
            ValueCodeBill.TabIndex = 54;
            ValueCodeBill.Text = ".";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label5.Location = new Point(3, 9);
            label5.Name = "label5";
            label5.Size = new Size(70, 20);
            label5.TabIndex = 27;
            label5.Text = "Code Bill";
            // 
            // Name
            // 
            Name.AutoSize = true;
            Name.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            Name.Location = new Point(3, 45);
            Name.Name = "Name";
            Name.Size = new Size(123, 20);
            Name.TabIndex = 29;
            Name.Text = "Employee Name";
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label11.Location = new Point(3, 195);
            label11.Name = "label11";
            label11.Size = new Size(0, 23);
            label11.TabIndex = 36;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label7.Location = new Point(3, 77);
            label7.Name = "label7";
            label7.Size = new Size(123, 20);
            label7.TabIndex = 31;
            label7.Text = "Customer Name";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label9.Location = new Point(3, 117);
            label9.Name = "label9";
            label9.Size = new Size(42, 20);
            label9.TabIndex = 33;
            label9.Text = "Date";
            // 
            // panel4
            // 
            panel4.BackColor = Color.Silver;
            panel4.Location = new Point(13, 281);
            panel4.Name = "panel4";
            panel4.Size = new Size(600, 2);
            panel4.TabIndex = 60;
            // 
            // panel2
            // 
            panel2.BackColor = Color.Silver;
            panel2.Location = new Point(13, 96);
            panel2.Name = "panel2";
            panel2.Size = new Size(600, 2);
            panel2.TabIndex = 61;
            // 
            // panel1
            // 
            panel1.BackColor = Color.Silver;
            panel1.Location = new Point(16, 546);
            panel1.Name = "panel1";
            panel1.Size = new Size(600, 2);
            panel1.TabIndex = 58;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 16.2F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(207, 39);
            label1.Name = "label1";
            label1.Size = new Size(219, 38);
            label1.TabIndex = 57;
            label1.Text = "THYNK TRAVEL";
            // 
            // pictureBox1
            // 
            pictureBox1.Image = (Image)resources.GetObject("pictureBox1.Image");
            pictureBox1.Location = new Point(13, 20);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new Size(84, 70);
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox1.TabIndex = 56;
            pictureBox1.TabStop = false;
            // 
            // printPreviewDialog1
            // 
            printPreviewDialog1.AutoScrollMargin = new Size(0, 0);
            printPreviewDialog1.AutoScrollMinSize = new Size(0, 0);
            printPreviewDialog1.ClientSize = new Size(400, 300);
            printPreviewDialog1.Enabled = true;
            printPreviewDialog1.Icon = (Icon)resources.GetObject("printPreviewDialog1.Icon");
            printPreviewDialog1.Name = "printPreviewDialog1";
            printPreviewDialog1.Visible = false;
            // 
            // pictureBox2
            // 
            pictureBox2.Image = Properties.Resources.icons8_print_24;
            pictureBox2.Location = new Point(601, 4);
            pictureBox2.Name = "pictureBox2";
            pictureBox2.Size = new Size(24, 25);
            pictureBox2.TabIndex = 66;
            pictureBox2.TabStop = false;
            pictureBox2.Click += pictureBox2_Click;
            // 
            // FormBill
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.White;
            ClientSize = new Size(649, 683);
            Controls.Add(pictureBox2);
            Controls.Add(panelPrint);
            Name = "FormBill";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "FormBill";
            panelPrint.ResumeLayout(false);
            panelPrint.PerformLayout();
            panel5.ResumeLayout(false);
            panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView1).EndInit();
            panel3.ResumeLayout(false);
            panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox2).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private Panel panelPrint;
        private PictureBox pictureBox2;
        private Panel panel6;
        private Label ValueTotalAmount;
        private Label label13;
        private Panel panel5;
        private DataGridView dataGridView1;
        private Label label4;
        private Panel panel3;
        private Label ValueDate;
        private Label ValueCustomerName;
        private Label ValueEmployeeName;
        private Label ValueCodeBill;
        private Label label5;
        private Label Name;
        private Label label11;
        private Label label7;
        private Label label9;
        private Panel panel4;
        private Panel panel2;
        private Panel panel1;
        private Label label1;
        private PictureBox pictureBox1;
        private PrintPreviewDialog printPreviewDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private ToolTip toolTip1;
    }
}