﻿namespace Projectc_
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            pictureBox_menumobile = new PictureBox();
            panel_openclosesidebar = new Panel();
            nightControlBox1 = new ReaLTaiizor.Controls.NightControlBox();
            panel_sidebar = new Panel();
            panel_containtbtnselect = new Panel();
            btnSetting = new Button();
            btnMessage = new Button();
            btnPersonel = new Button();
            btnClients = new Button();
            btnFlight = new Button();
            btnDashBoard = new Button();
            panel_containlogo = new Panel();
            pictureBox_logo = new PictureBox();
            panel_body = new Panel();
            ((System.ComponentModel.ISupportInitialize)pictureBox_menumobile).BeginInit();
            panel_openclosesidebar.SuspendLayout();
            panel_sidebar.SuspendLayout();
            panel_containtbtnselect.SuspendLayout();
            panel_containlogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox_logo).BeginInit();
            SuspendLayout();
            // 
            // pictureBox_menumobile
            // 
            pictureBox_menumobile.Image = (Image)resources.GetObject("pictureBox_menumobile.Image");
            pictureBox_menumobile.Location = new Point(3, 3);
            pictureBox_menumobile.Name = "pictureBox_menumobile";
            pictureBox_menumobile.Size = new Size(48, 42);
            pictureBox_menumobile.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox_menumobile.TabIndex = 0;
            pictureBox_menumobile.TabStop = false;
            pictureBox_menumobile.Click += pictureBox_menumobile_Click;
            // 
            // panel_openclosesidebar
            // 
            panel_openclosesidebar.BackColor = Color.White;
            panel_openclosesidebar.Controls.Add(nightControlBox1);
            panel_openclosesidebar.Controls.Add(pictureBox_menumobile);
            panel_openclosesidebar.Dock = DockStyle.Top;
            panel_openclosesidebar.Location = new Point(0, 0);
            panel_openclosesidebar.Name = "panel_openclosesidebar";
            panel_openclosesidebar.Size = new Size(1713, 49);
            panel_openclosesidebar.TabIndex = 2;
            panel_openclosesidebar.MouseDown += panel_openclosesidebar_MouseDown_1;
            // 
            // nightControlBox1
            // 
            nightControlBox1.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            nightControlBox1.BackColor = Color.Transparent;
            nightControlBox1.CloseHoverColor = Color.FromArgb(199, 80, 80);
            nightControlBox1.CloseHoverForeColor = Color.White;
            nightControlBox1.DefaultLocation = true;
            nightControlBox1.DisableMaximizeColor = Color.FromArgb(105, 105, 105);
            nightControlBox1.DisableMinimizeColor = Color.FromArgb(105, 105, 105);
            nightControlBox1.EnableCloseColor = Color.FromArgb(160, 160, 160);
            nightControlBox1.EnableMaximizeButton = true;
            nightControlBox1.EnableMaximizeColor = Color.FromArgb(160, 160, 160);
            nightControlBox1.EnableMinimizeButton = true;
            nightControlBox1.EnableMinimizeColor = Color.FromArgb(160, 160, 160);
            nightControlBox1.Location = new Point(1574, 0);
            nightControlBox1.MaximizeHoverColor = Color.FromArgb(15, 255, 255, 255);
            nightControlBox1.MaximizeHoverForeColor = Color.White;
            nightControlBox1.MinimizeHoverColor = Color.FromArgb(15, 255, 255, 255);
            nightControlBox1.MinimizeHoverForeColor = Color.White;
            nightControlBox1.Name = "nightControlBox1";
            nightControlBox1.Size = new Size(139, 31);
            nightControlBox1.TabIndex = 1;
            // 
            // panel_sidebar
            // 
            panel_sidebar.BackColor = Color.FromArgb(8, 8, 8);
            panel_sidebar.Controls.Add(panel_containtbtnselect);
            panel_sidebar.Controls.Add(panel_containlogo);
            panel_sidebar.Dock = DockStyle.Left;
            panel_sidebar.Location = new Point(0, 49);
            panel_sidebar.Name = "panel_sidebar";
            panel_sidebar.Size = new Size(240, 947);
            panel_sidebar.TabIndex = 3;
            // 
            // panel_containtbtnselect
            // 
            panel_containtbtnselect.BackColor = Color.FromArgb(8, 8, 8);
            panel_containtbtnselect.Controls.Add(btnSetting);
            panel_containtbtnselect.Controls.Add(btnMessage);
            panel_containtbtnselect.Controls.Add(btnPersonel);
            panel_containtbtnselect.Controls.Add(btnClients);
            panel_containtbtnselect.Controls.Add(btnFlight);
            panel_containtbtnselect.Controls.Add(btnDashBoard);
            panel_containtbtnselect.Location = new Point(3, 156);
            panel_containtbtnselect.Name = "panel_containtbtnselect";
            panel_containtbtnselect.Size = new Size(234, 437);
            panel_containtbtnselect.TabIndex = 4;
            // 
            // btnSetting
            // 
            btnSetting.BackColor = Color.FromArgb(8, 8, 8);
            btnSetting.FlatAppearance.BorderSize = 0;
            btnSetting.FlatAppearance.MouseOverBackColor = Color.FromArgb(50, 50, 60);
            btnSetting.FlatStyle = FlatStyle.Flat;
            btnSetting.Font = new Font("Verdana", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btnSetting.ForeColor = Color.White;
            btnSetting.Image = (Image)resources.GetObject("btnSetting.Image");
            btnSetting.ImageAlign = ContentAlignment.MiddleLeft;
            btnSetting.Location = new Point(3, 341);
            btnSetting.Name = "btnSetting";
            btnSetting.Size = new Size(231, 50);
            btnSetting.TabIndex = 5;
            btnSetting.Text = "Setting";
            btnSetting.UseVisualStyleBackColor = false;
            btnSetting.Click += btnSetting_Click;
            // 
            // btnMessage
            // 
            btnMessage.BackColor = Color.FromArgb(8, 8, 8);
            btnMessage.FlatAppearance.BorderSize = 0;
            btnMessage.FlatAppearance.MouseOverBackColor = Color.FromArgb(50, 50, 60);
            btnMessage.FlatStyle = FlatStyle.Flat;
            btnMessage.Font = new Font("Verdana", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btnMessage.ForeColor = Color.White;
            btnMessage.Image = (Image)resources.GetObject("btnMessage.Image");
            btnMessage.ImageAlign = ContentAlignment.MiddleLeft;
            btnMessage.Location = new Point(3, 274);
            btnMessage.Name = "btnMessage";
            btnMessage.Size = new Size(231, 50);
            btnMessage.TabIndex = 4;
            btnMessage.Text = "Invoice";
            btnMessage.UseVisualStyleBackColor = false;
            btnMessage.Click += btnMessage_Click;
            // 
            // btnPersonel
            // 
            btnPersonel.BackColor = Color.FromArgb(8, 8, 8);
            btnPersonel.FlatAppearance.BorderSize = 0;
            btnPersonel.FlatAppearance.MouseOverBackColor = Color.FromArgb(50, 50, 60);
            btnPersonel.FlatStyle = FlatStyle.Flat;
            btnPersonel.Font = new Font("Verdana", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btnPersonel.ForeColor = Color.White;
            btnPersonel.Image = (Image)resources.GetObject("btnPersonel.Image");
            btnPersonel.ImageAlign = ContentAlignment.MiddleLeft;
            btnPersonel.Location = new Point(3, 208);
            btnPersonel.Name = "btnPersonel";
            btnPersonel.Size = new Size(231, 50);
            btnPersonel.TabIndex = 3;
            btnPersonel.Text = "Personel";
            btnPersonel.UseVisualStyleBackColor = false;
            btnPersonel.Click += btnMySite_Click;
            // 
            // btnClients
            // 
            btnClients.BackColor = Color.FromArgb(8, 8, 8);
            btnClients.FlatAppearance.BorderSize = 0;
            btnClients.FlatAppearance.MouseOverBackColor = Color.FromArgb(50, 50, 60);
            btnClients.FlatStyle = FlatStyle.Flat;
            btnClients.Font = new Font("Verdana", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btnClients.ForeColor = Color.White;
            btnClients.Image = (Image)resources.GetObject("btnClients.Image");
            btnClients.ImageAlign = ContentAlignment.MiddleLeft;
            btnClients.Location = new Point(3, 143);
            btnClients.Name = "btnClients";
            btnClients.Size = new Size(231, 50);
            btnClients.TabIndex = 2;
            btnClients.Text = "Clients";
            btnClients.UseVisualStyleBackColor = false;
            btnClients.Click += btnClients_Click;
            // 
            // btnFlight
            // 
            btnFlight.BackColor = Color.FromArgb(8, 8, 8);
            btnFlight.FlatAppearance.BorderSize = 0;
            btnFlight.FlatAppearance.MouseOverBackColor = Color.FromArgb(50, 50, 60);
            btnFlight.FlatStyle = FlatStyle.Flat;
            btnFlight.Font = new Font("Verdana", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btnFlight.ForeColor = Color.White;
            btnFlight.Image = (Image)resources.GetObject("btnFlight.Image");
            btnFlight.ImageAlign = ContentAlignment.MiddleLeft;
            btnFlight.Location = new Point(3, 71);
            btnFlight.Name = "btnFlight";
            btnFlight.Size = new Size(231, 50);
            btnFlight.TabIndex = 1;
            btnFlight.Text = "Flight";
            btnFlight.UseVisualStyleBackColor = false;
            btnFlight.Click += btnFlight_Click;
            // 
            // btnDashBoard
            // 
            btnDashBoard.BackColor = Color.FromArgb(8, 8, 8);
            btnDashBoard.FlatAppearance.BorderSize = 0;
            btnDashBoard.FlatAppearance.MouseOverBackColor = Color.FromArgb(50, 50, 60);
            btnDashBoard.FlatStyle = FlatStyle.Flat;
            btnDashBoard.Font = new Font("Verdana", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btnDashBoard.ForeColor = Color.White;
            btnDashBoard.Image = (Image)resources.GetObject("btnDashBoard.Image");
            btnDashBoard.ImageAlign = ContentAlignment.MiddleLeft;
            btnDashBoard.Location = new Point(3, 3);
            btnDashBoard.Name = "btnDashBoard";
            btnDashBoard.Size = new Size(231, 50);
            btnDashBoard.TabIndex = 0;
            btnDashBoard.Text = "DashBoard";
            btnDashBoard.UseVisualStyleBackColor = false;
            btnDashBoard.Click += btnDashBoard_Click;
            // 
            // panel_containlogo
            // 
            panel_containlogo.Controls.Add(pictureBox_logo);
            panel_containlogo.Location = new Point(0, 3);
            panel_containlogo.Name = "panel_containlogo";
            panel_containlogo.Size = new Size(237, 147);
            panel_containlogo.TabIndex = 4;
            // 
            // pictureBox_logo
            // 
            pictureBox_logo.Dock = DockStyle.Fill;
            pictureBox_logo.Image = (Image)resources.GetObject("pictureBox_logo.Image");
            pictureBox_logo.Location = new Point(0, 0);
            pictureBox_logo.Name = "pictureBox_logo";
            pictureBox_logo.Size = new Size(237, 147);
            pictureBox_logo.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox_logo.TabIndex = 0;
            pictureBox_logo.TabStop = false;
            // 
            // panel_body
            // 
            panel_body.Dock = DockStyle.Fill;
            panel_body.Location = new Point(240, 49);
            panel_body.Name = "panel_body";
            panel_body.Size = new Size(1473, 947);
            panel_body.TabIndex = 4;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.FromArgb(240, 238, 239);
            ClientSize = new Size(1713, 996);
            Controls.Add(panel_body);
            Controls.Add(panel_sidebar);
            Controls.Add(panel_openclosesidebar);
            FormBorderStyle = FormBorderStyle.None;
            Name = "Form1";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)pictureBox_menumobile).EndInit();
            panel_openclosesidebar.ResumeLayout(false);
            panel_sidebar.ResumeLayout(false);
            panel_containtbtnselect.ResumeLayout(false);
            panel_containlogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)pictureBox_logo).EndInit();
            ResumeLayout(false);
        }

        #endregion
        private PictureBox pictureBox_menumobile;
        private Panel panel_openclosesidebar;
        private ReaLTaiizor.Controls.NightControlBox nightControlBox1;
        private Panel panel_sidebar;
        private Panel panel_containlogo;
        private PictureBox pictureBox_logo;
        private Panel panel_containtbtnselect;
        private Button btnFlight;
        private Button btnDashBoard;
        private Button btnClients;
        private Button btnPersonel;
        private Button btnSetting;
        private Button btnMessage;
        private Panel panel_body;
    }
}