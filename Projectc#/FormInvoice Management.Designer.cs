﻿namespace Projectc_
{
    partial class FormInvoice_Management
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormInvoice_Management));
            btnLoad = new Button();
            panel2 = new Panel();
            btnSearch = new Button();
            txtSearch = new TextBox();
            label2 = new Label();
            panel1 = new Panel();
            btnDelete = new Button();
            btnEdit = new Button();
            btnAdd = new Button();
            txtIntoMoney = new TextBox();
            label3 = new Label();
            txtTicketTypeCode = new TextBox();
            txtPromotionalCode = new TextBox();
            label11 = new Label();
            txtTax = new TextBox();
            label10 = new Label();
            label9 = new Label();
            txtQuatity = new TextBox();
            label7 = new Label();
            txtCodeBill = new TextBox();
            Name = new Label();
            txtCustomerTicketCode = new TextBox();
            label5 = new Label();
            label4 = new Label();
            btnPrint = new Button();
            dataGridViewClients = new DataGridView();
            label1 = new Label();
            dataGridView1 = new DataGridView();
            panel2.SuspendLayout();
            panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridViewClients).BeginInit();
            ((System.ComponentModel.ISupportInitialize)dataGridView1).BeginInit();
            SuspendLayout();
            // 
            // btnLoad
            // 
            btnLoad.Anchor = AnchorStyles.Top;
            btnLoad.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btnLoad.Image = (Image)resources.GetObject("btnLoad.Image");
            btnLoad.ImageAlign = ContentAlignment.MiddleLeft;
            btnLoad.Location = new Point(688, 163);
            btnLoad.Name = "btnLoad";
            btnLoad.Size = new Size(164, 36);
            btnLoad.TabIndex = 14;
            btnLoad.Text = "Load";
            btnLoad.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            panel2.Anchor = AnchorStyles.Top;
            panel2.Controls.Add(btnSearch);
            panel2.Controls.Add(txtSearch);
            panel2.Controls.Add(label2);
            panel2.Location = new Point(12, 64);
            panel2.Name = "panel2";
            panel2.Size = new Size(1428, 58);
            panel2.TabIndex = 13;
            // 
            // btnSearch
            // 
            btnSearch.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btnSearch.Image = (Image)resources.GetObject("btnSearch.Image");
            btnSearch.ImageAlign = ContentAlignment.MiddleLeft;
            btnSearch.Location = new Point(403, 6);
            btnSearch.Name = "btnSearch";
            btnSearch.Size = new Size(143, 35);
            btnSearch.TabIndex = 3;
            btnSearch.Text = "Search";
            btnSearch.UseVisualStyleBackColor = true;
            // 
            // txtSearch
            // 
            txtSearch.Font = new Font("Segoe UI Light", 9F, FontStyle.Italic, GraphicsUnit.Point);
            txtSearch.Location = new Point(84, 11);
            txtSearch.Name = "txtSearch";
            txtSearch.Size = new Size(297, 27);
            txtSearch.TabIndex = 2;
            txtSearch.Text = "ID";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point);
            label2.Location = new Point(3, 10);
            label2.Name = "label2";
            label2.Size = new Size(75, 28);
            label2.TabIndex = 1;
            label2.Text = "Search";
            // 
            // panel1
            // 
            panel1.Anchor = AnchorStyles.Top;
            panel1.Controls.Add(btnDelete);
            panel1.Controls.Add(btnEdit);
            panel1.Controls.Add(btnAdd);
            panel1.Controls.Add(txtIntoMoney);
            panel1.Controls.Add(label3);
            panel1.Controls.Add(txtTicketTypeCode);
            panel1.Controls.Add(txtPromotionalCode);
            panel1.Controls.Add(label11);
            panel1.Controls.Add(txtTax);
            panel1.Controls.Add(label10);
            panel1.Controls.Add(label9);
            panel1.Controls.Add(txtQuatity);
            panel1.Controls.Add(label7);
            panel1.Controls.Add(txtCodeBill);
            panel1.Controls.Add(Name);
            panel1.Controls.Add(txtCustomerTicketCode);
            panel1.Controls.Add(label5);
            panel1.Controls.Add(label4);
            panel1.Location = new Point(877, 180);
            panel1.Name = "panel1";
            panel1.Size = new Size(545, 602);
            panel1.TabIndex = 12;
            // 
            // btnDelete
            // 
            btnDelete.Font = new Font("Segoe UI", 10.8F, FontStyle.Bold, GraphicsUnit.Point);
            btnDelete.Image = (Image)resources.GetObject("btnDelete.Image");
            btnDelete.ImageAlign = ContentAlignment.MiddleLeft;
            btnDelete.Location = new Point(380, 512);
            btnDelete.Name = "btnDelete";
            btnDelete.Size = new Size(145, 36);
            btnDelete.TabIndex = 28;
            btnDelete.Text = "    Delete";
            btnDelete.UseVisualStyleBackColor = true;
            // 
            // btnEdit
            // 
            btnEdit.Font = new Font("Segoe UI", 10.8F, FontStyle.Bold, GraphicsUnit.Point);
            btnEdit.Image = (Image)resources.GetObject("btnEdit.Image");
            btnEdit.ImageAlign = ContentAlignment.MiddleLeft;
            btnEdit.Location = new Point(196, 512);
            btnEdit.Name = "btnEdit";
            btnEdit.Size = new Size(145, 36);
            btnEdit.TabIndex = 27;
            btnEdit.Text = "    Edit";
            btnEdit.UseVisualStyleBackColor = true;
            // 
            // btnAdd
            // 
            btnAdd.Font = new Font("Segoe UI", 10.8F, FontStyle.Bold, GraphicsUnit.Point);
            btnAdd.Image = (Image)resources.GetObject("btnAdd.Image");
            btnAdd.ImageAlign = ContentAlignment.MiddleLeft;
            btnAdd.Location = new Point(15, 512);
            btnAdd.Name = "btnAdd";
            btnAdd.Size = new Size(145, 36);
            btnAdd.TabIndex = 24;
            btnAdd.Text = "    Add";
            btnAdd.UseVisualStyleBackColor = true;
            // 
            // txtIntoMoney
            // 
            txtIntoMoney.Location = new Point(215, 423);
            txtIntoMoney.Name = "txtIntoMoney";
            txtIntoMoney.Size = new Size(310, 27);
            txtIntoMoney.TabIndex = 26;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label3.Location = new Point(12, 427);
            label3.Name = "label3";
            label3.Size = new Size(101, 23);
            label3.TabIndex = 25;
            label3.Text = "Into Money";
            // 
            // txtTicketTypeCode
            // 
            txtTicketTypeCode.Location = new Point(215, 248);
            txtTicketTypeCode.Name = "txtTicketTypeCode";
            txtTicketTypeCode.Size = new Size(309, 27);
            txtTicketTypeCode.TabIndex = 24;
            // 
            // txtPromotionalCode
            // 
            txtPromotionalCode.Location = new Point(215, 363);
            txtPromotionalCode.Name = "txtPromotionalCode";
            txtPromotionalCode.Size = new Size(310, 27);
            txtPromotionalCode.TabIndex = 21;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label11.Location = new Point(12, 367);
            label11.Name = "label11";
            label11.Size = new Size(155, 23);
            label11.TabIndex = 20;
            label11.Text = "Promotional Code";
            // 
            // txtTax
            // 
            txtTax.Location = new Point(214, 307);
            txtTax.Name = "txtTax";
            txtTax.Size = new Size(310, 27);
            txtTax.TabIndex = 19;
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label10.Location = new Point(12, 307);
            label10.Name = "label10";
            label10.Size = new Size(37, 23);
            label10.TabIndex = 18;
            label10.Text = "Tax";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label9.Location = new Point(12, 248);
            label9.Name = "label9";
            label9.Size = new Size(148, 23);
            label9.TabIndex = 16;
            label9.Text = "Ticket Type Code";
            // 
            // txtQuatity
            // 
            txtQuatity.Location = new Point(215, 189);
            txtQuatity.Name = "txtQuatity";
            txtQuatity.Size = new Size(309, 27);
            txtQuatity.TabIndex = 13;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label7.Location = new Point(12, 193);
            label7.Name = "label7";
            label7.Size = new Size(80, 23);
            label7.TabIndex = 12;
            label7.Text = "Quantity";
            // 
            // txtCodeBill
            // 
            txtCodeBill.Location = new Point(215, 132);
            txtCodeBill.Name = "txtCodeBill";
            txtCodeBill.Size = new Size(309, 27);
            txtCodeBill.TabIndex = 11;
            // 
            // Name
            // 
            Name.AutoSize = true;
            Name.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            Name.Location = new Point(12, 136);
            Name.Name = "Name";
            Name.Size = new Size(82, 23);
            Name.TabIndex = 10;
            Name.Text = "Code Bill";
            // 
            // txtCustomerTicketCode
            // 
            txtCustomerTicketCode.Location = new Point(215, 73);
            txtCustomerTicketCode.Name = "txtCustomerTicketCode";
            txtCustomerTicketCode.Size = new Size(309, 27);
            txtCustomerTicketCode.TabIndex = 9;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            label5.Location = new Point(12, 77);
            label5.Name = "label5";
            label5.Size = new Size(187, 23);
            label5.TabIndex = 8;
            label5.Text = "Customer Ticket Code";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Segoe UI", 15F, FontStyle.Bold, GraphicsUnit.Point);
            label4.Location = new Point(174, 9);
            label4.Name = "label4";
            label4.Size = new Size(184, 35);
            label4.TabIndex = 8;
            label4.Text = "Invoice details";
            // 
            // btnPrint
            // 
            btnPrint.Anchor = AnchorStyles.Top;
            btnPrint.Font = new Font("Segoe UI", 10.8F, FontStyle.Bold, GraphicsUnit.Point);
            btnPrint.Image = (Image)resources.GetObject("btnPrint.Image");
            btnPrint.ImageAlign = ContentAlignment.MiddleLeft;
            btnPrint.Location = new Point(489, 163);
            btnPrint.Name = "btnPrint";
            btnPrint.Size = new Size(193, 36);
            btnPrint.TabIndex = 23;
            btnPrint.Text = "    Print";
            btnPrint.UseVisualStyleBackColor = true;
            btnPrint.MouseClick += btnPrint_MouseClick;
            // 
            // dataGridViewClients
            // 
            dataGridViewClients.Anchor = AnchorStyles.Top;
            dataGridViewClients.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewClients.Location = new Point(15, 205);
            dataGridViewClients.Name = "dataGridViewClients";
            dataGridViewClients.RowHeadersWidth = 51;
            dataGridViewClients.RowTemplate.Height = 29;
            dataGridViewClients.Size = new Size(837, 375);
            dataGridViewClients.TabIndex = 11;
            // 
            // label1
            // 
            label1.Anchor = AnchorStyles.Top;
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 22.2F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(-1, -2);
            label1.Name = "label1";
            label1.Size = new Size(388, 50);
            label1.TabIndex = 10;
            label1.Text = "Invoice Management";
            // 
            // dataGridView1
            // 
            dataGridView1.Anchor = AnchorStyles.Top;
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridView1.Location = new Point(15, 607);
            dataGridView1.Name = "dataGridView1";
            dataGridView1.RowHeadersWidth = 51;
            dataGridView1.RowTemplate.Height = 29;
            dataGridView1.Size = new Size(837, 315);
            dataGridView1.TabIndex = 15;
            // 
            // FormInvoice_Management
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1455, 949);
            Controls.Add(dataGridView1);
            Controls.Add(btnLoad);
            Controls.Add(panel2);
            Controls.Add(btnPrint);
            Controls.Add(panel1);
            Controls.Add(dataGridViewClients);
            Controls.Add(label1);
            Text = "FormInvoice_Management";
            panel2.ResumeLayout(false);
            panel2.PerformLayout();
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridViewClients).EndInit();
            ((System.ComponentModel.ISupportInitialize)dataGridView1).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnLoad;
        private Panel panel2;
        private Button btnSearch;
        private TextBox txtSearch;
        private Label label2;
        private Panel panel1;
        private TextBox txtTicketTypeCode;
        private Button btnPrint;
        private TextBox txtPromotionalCode;
        private Label label11;
        private TextBox txtTax;
        private Label label10;
        private Label label9;
        private TextBox txtQuatity;
        private Label label7;
        private TextBox txtCodeBill;
        private Label Name;
        private TextBox txtCustomerTicketCode;
        private Label label5;
        private Label label4;
        private DataGridView dataGridViewClients;
        private Label label1;
        private TextBox txtIntoMoney;
        private Label label3;
        private Button btnAdd;
        private DataGridView dataGridView1;
        private Button btnDelete;
        private Button btnEdit;
    }
}