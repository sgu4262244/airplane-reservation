﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projectc_
{
    public partial class FormInvoice_Management : Form
    {
        public FormInvoice_Management()
        {
            InitializeComponent();
        }

        private void btnPrint_MouseClick(object sender, MouseEventArgs e)
        {
            FormBill form = new FormBill();
            form.Show();
        }
    }
}
