using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
namespace Projectc_
{
    public partial class Form1 : System.Windows.Forms.Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        [DllImport("user32.dll", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.dll", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
        private void panel_openclosesidebar_MouseDown_1(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel_sidebar_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox_menumobile_Click(object sender, EventArgs e)
        {
            if (panel_sidebar.Width == 240)
            {
                panel_sidebar.Width = 55;
            }
            else
            {
                panel_sidebar.Width = 240;
            }
        }
        Button previousButton = null;
        private void ChangeButtonColor(Button currentButton)
        {
            if (previousButton != null)
            {
                previousButton.BackColor = Color.FromArgb(8, 8, 8); // ??t m�u n?n n�t tr??c ?�
            }

            currentButton.BackColor = Color.FromArgb(50, 50, 60); // ??t m�u n?n n�t hi?n t?i

            previousButton = currentButton; // C?p nh?t n�t tr??c ?�
        }

        private Form currentFromChild;
        private void OpenChildForm(Form Child)
        {
            if (currentFromChild != null)
            {
                currentFromChild.Close();
            }
            currentFromChild = Child;
            Child.TopLevel = false;
            Child.FormBorderStyle = FormBorderStyle.None;
            Child.Dock = DockStyle.Fill;
            panel_body.Controls.Add(Child);
            panel_body.Tag = Child;
            Child.BringToFront();
            Child.Show();
        }
        private void btnDashBoard_Click(object sender, EventArgs e)
        {
            ChangeButtonColor(btnDashBoard);
            OpenChildForm(new Form_DashBoard());
        }

        private void btnFlight_Click(object sender, EventArgs e)
        {
            ChangeButtonColor(btnFlight);
            OpenChildForm(new Form_Flight());
        }

        private void btnClients_Click(object sender, EventArgs e)
        {
            ChangeButtonColor(btnClients);
            OpenChildForm(new FormClient());
        }

        private void btnMySite_Click(object sender, EventArgs e)
        {
            ChangeButtonColor(btnPersonel);
            OpenChildForm(new FormPersonel());
        }

        private void btnMessage_Click(object sender, EventArgs e)
        {
            ChangeButtonColor(btnMessage);
            OpenChildForm(new FormInvoice_Management());
        }

        private void btnSetting_Click(object sender, EventArgs e)
        {
            ChangeButtonColor(btnSetting);
        }


    }
}